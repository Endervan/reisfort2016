-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 26/06/2017 às 09:52
-- Versão do servidor: 5.5.51-38.2
-- Versão do PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `reisfort_bd`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_atuacoes`
--

CREATE TABLE IF NOT EXISTS `tb_atuacoes` (
  `idatuacao` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idatuacao`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=154 ;

--
-- Fazendo dump de dados para tabela `tb_atuacoes`
--

INSERT INTO `tb_atuacoes` (`idatuacao`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `cargo`, `telefone`, `email`, `facebook`) VALUES
(150, 'Indústria Energética', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-energética', NULL, NULL, NULL, NULL, '', '', '', NULL),
(145, 'Indústria de Exploração', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-de-exploração', NULL, NULL, NULL, NULL, '', '', '', NULL),
(146, 'Indústria de Extração', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-de-extração', NULL, NULL, NULL, NULL, '', '', '', NULL),
(147, 'Setor Automotivo', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'setor-automotivo', NULL, NULL, NULL, NULL, '', '', '', NULL),
(148, 'Indústria Alimentícia', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-alimentícia', NULL, NULL, NULL, NULL, '', '', '', NULL),
(149, 'Setor Agrícola', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'setor-agrícola', NULL, NULL, NULL, NULL, '', '', '', NULL),
(152, 'Infraestrutura', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'infraestrutura', NULL, NULL, NULL, NULL, '', '', '', NULL),
(153, 'Transporte Pesado', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'transporte-pesado', NULL, NULL, NULL, NULL, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idavaliacaoproduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_clientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=9 ;

--
-- Fazendo dump de dados para tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `imagem_clientes`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Banner Index 1', '0311201607147789450784.jpg', NULL, 'SIM', NULL, '1', 'banner-index-1', '/produtos', NULL, NULL),
(2, 'Banner Index 2', '2809201601241231229283.jpg', NULL, 'SIM', NULL, '1', 'banner-index-2', '/produtos/banheiros-quimicos/', NULL, NULL),
(3, 'Banner Index 3', '0311201607153880302322.jpg', NULL, 'SIM', NULL, '1', 'banner-index-3', '/produtos', NULL, NULL),
(4, 'Banner Index 4', '1910201611337118727461.jpg', NULL, 'NAO', NULL, '1', 'banner-index-4', '', NULL, NULL),
(5, 'Mobile index 01', '0311201607209372707969.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-01', '/produtos', NULL, NULL),
(6, 'Mobile index 02', '3110201612536432854951.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-02', '/produtos/banheiros-quimicos', NULL, NULL),
(7, 'Mobile index 03', '0311201607364971821233.jpg', NULL, 'SIM', NULL, '2', 'mobile-index-03', '/produtos', NULL, NULL),
(8, 'Mobile Index 04', '0410201607381188167018.jpg', NULL, 'NAO', NULL, '2', 'mobile-index-04', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_banners_internas`
--

CREATE TABLE IF NOT EXISTS `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idbannerinterna`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=21 ;

--
-- Fazendo dump de dados para tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '2909201608331144712449.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Produtos', '2909201610531396927767.jpg', NULL, 'produtos', 'SIM', NULL),
(3, 'Produtos Dentro', '2909201606401299353420.jpg', NULL, 'produtos-dentro', 'SIM', NULL),
(4, 'Serviços', NULL, NULL, NULL, 'SIM', NULL),
(5, 'Serviços Dentro', NULL, NULL, NULL, 'SIM', NULL),
(6, 'Notícias', '3009201605181148024230.jpg', NULL, 'noticias', 'SIM', NULL),
(7, 'Notícias Dentro', '3009201607201313677097.jpg', NULL, 'noticias-dentro', 'SIM', NULL),
(8, 'Orçamentos', '3009201609431229696558.jpg', NULL, 'orcamentos', 'SIM', NULL),
(9, 'Fale Conosco', '3009201611131191752499.jpg', NULL, 'fale-conosco', 'SIM', NULL),
(10, 'Trabalhe Conosco', '3009201601271202810013.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(11, 'Mobile Empresa', '0510201603381182227710.jpg', NULL, 'mobile-empresa', 'SIM', NULL),
(12, 'Mobile Produtos', '0610201610201262120984.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(13, 'Mobile Produtos Dentro', '0610201605081341840913.jpg', NULL, 'mobile-produtos-dentro', 'SIM', NULL),
(14, 'Mobile Serviços', NULL, NULL, NULL, 'SIM', NULL),
(15, 'Mobile Serviços Dentro', NULL, NULL, NULL, 'SIM', NULL),
(16, 'Mobile Notícias', '0510201605341351101125.jpg', NULL, 'mobile-noticias', 'SIM', NULL),
(17, 'Mobile Notícias Dentro', '0510201606001256847681.jpg', NULL, 'mobile-noticias-dentro', 'SIM', NULL),
(18, 'Mobile Orçamentos', '0710201601511222306636.jpg', NULL, 'mobile-orcamentos', 'SIM', NULL),
(19, 'Mobile Fale Conosco', '0510201604591129312216.jpg', NULL, 'mobile-fale-conosco', 'SIM', NULL),
(20, 'Mobile Trabalhe Conosco', '0510201604591325181656.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaproduto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=95 ;

--
-- Fazendo dump de dados para tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'TENDAS', NULL, '2809201605571329159441.png', 'SIM', NULL, 'tendas', '', '', ''),
(89, 'BANHEIROS QUÍMICOS', NULL, '2809201605571392302223.png', 'SIM', NULL, 'banheiros-quimicos', '', '', ''),
(90, 'BALCÕES', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'balcoes', NULL, NULL, NULL),
(91, 'CONTAINERS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'containers', NULL, NULL, NULL),
(92, 'PISOS E PALCOS', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'pisos-e-palcos', NULL, NULL, NULL),
(93, 'BARRICADAS / GRADIL', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'barricadas--gradil', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_clientes`
--

CREATE TABLE IF NOT EXISTS `tb_clientes` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=23 ;

--
-- Fazendo dump de dados para tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(19, 'clientes 01 teste', NULL, '1509201609231327508113..jpg', 'SIM', NULL, 'clientes-01-teste', '', '', '', ''),
(20, 'cliente 02', NULL, '1509201609241214652136..jpg', 'SIM', NULL, 'cliente-02', '', '', '', ''),
(21, 'cliente 03', NULL, '1509201609271396809164..jpg', 'SIM', NULL, 'cliente-03', '', '', '', ''),
(22, 'cliente03', NULL, '1509201609331123555375..jpg', 'SIM', NULL, 'cliente03', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentariodica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentarioproduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `imagem` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`, `imagem`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Avenida Araxá Qd.19 Lt 03/04 nº 555 Jardim Ana Lúcia - Goiânia - GO', '3247-4118', '98107-6597', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3821.593224397298!2d-49.31611958513323!3d-16.69722688849571!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xec317843a30831cf!2sReisforts+Tendas+e+Banheiros+Qu%C3%ADmicos!5e0!3m2!1spt-BR!2sus!', NULL, NULL, 'marciomas@gmail.com, angela.homeweb@gmail.com, renato@reisforts.com.br, administrativo@reisforts.com.br,comercial@reisforts.com.br, locacao@reisforts.com.br', 'https://plus.google.com/104673852911843518726', '3945-4291', '', '(62)', '(62)', '62', '', 'https://pt-br.facebook.com/ReisForts/', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=45 ;

--
-- Fazendo dump de dados para tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(1, '6 DICAS teste cadastro', '<p>\r\n	<strong>Correia falsificada pode causar danos ao motor</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Propriet&aacute;rios de ve&iacute;culos, profissionais e varejistas do setor automotivo devem ficar atentos: as autope&ccedil;as est&atilde;o em primeiro lugar no ranking de falsifica&ccedil;&otilde;es, segundo a ABCF (Associa&ccedil;&atilde;o Brasileira de Combate &agrave; Falsifica&ccedil;&atilde;o). Al&eacute;m de afetar o mercado, com preju&iacute;zos de R$ 3 bilh&otilde;es ao ano, essa pr&aacute;tica coloca vidas em risco, pois produtos pirateados n&atilde;o atendem aos padr&otilde;es de engenharia e normas t&eacute;cnicas de seguran&ccedil;a. Uma correia dentada original, por exemplo, &eacute; fabricada com alto n&iacute;vel de tecnologia embarcada e projetada para resistir &agrave;s situa&ccedil;&otilde;es de uso mais extremas do motor.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Segundo a ContiTech, a correia falsificada pode causar danos ao motor, como empenamento das v&aacute;lvulas de admiss&atilde;o e escape e desgaste no cabe&ccedil;ote. A quebra deste componente pode gerar um alto custo financeiro para consertar o motor, al&eacute;m dos dias sem carro. E, mais grave, se a correia quebrar com o ve&iacute;culo em movimento, o risco de acidente &eacute; enorme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para evitar problemas, a ContiTech, marca do Grupo Continental, um dos maiores fornecedores mundiais da ind&uacute;stria automobil&iacute;stica, elenca seis pontos que devem ser observados ao adquirir uma correia automotiva.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Conferir o n&uacute;mero de s&eacute;rie. Cada pe&ccedil;a possui um &uacute;nico e espec&iacute;fico n&uacute;mero de s&eacute;rie. O ideal &eacute; comparar com as demais pe&ccedil;as da loja ou oficina, para verificar se o n&uacute;mero &eacute; exclusivo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	2. Checar o c&oacute;digo exclusivo. As pe&ccedil;as da ContiTech possuem um c&oacute;digo chamado Data Matrix, uma esp&eacute;cie de QR Code. Com um smartphone &eacute; poss&iacute;vel fazer a leitura do c&oacute;digo. Se o celular n&atilde;o reconhecer, significa que a pe&ccedil;a &eacute; falsa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	3. Comparar os dados. Ao abrir o Data Matrix no celular, aparecer&aacute; uma sequ&ecirc;ncia de n&uacute;meros. Basta comparar os primeiros d&iacute;gitos com o n&uacute;mero de s&eacute;rie da pe&ccedil;a. Eles precisam ser iguais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	4. Verificar a embalagem. Correias dentadas originais de f&aacute;brica s&atilde;o embaladas em caixas lacradas. Nunca v&ecirc;m soltas, em embalagens pl&aacute;sticas ou presas com fita adesiva. J&aacute; as correias V e Multi V da ContiTech s&atilde;o apenas envolvidas por uma embalagem chamada luva. Fique atento!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	5. Pesquisar os pre&ccedil;os. Pe&ccedil;as baratas demais devem ser avaliadas. &Eacute; importante pesquisar pre&ccedil;os e desconfiar de produtos cujo valor esteja muito abaixo do praticado no mercado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	6. Exigir nota fiscal. Esta &eacute; outra forma de comprovar a autenticidade da pe&ccedil;a. E mais: com ela, &eacute; poss&iacute;vel reivindicar a garantia do produto em caso de defeito.</p>', '1609201609381398670329..jpg', 'SIM', NULL, '6-dicas-teste-cadastro', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(2, 'dica teste final', '<p>\r\n	++++teste final +++++++Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<div>\r\n	&nbsp;</div>', '1609201609411386919440..jpg', 'SIM', NULL, 'dica-teste-final', '', '', '', NULL),
(44, 'teste cadastro 01 dicas geral slider', '<p>\r\n	descricao teste cadastro 01 dicas geral slider</p>', '2109201602581371500960..jpg', 'SIM', NULL, 'teste-cadastro-01-dicas-geral-slider', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Fazendo dump de dados para tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'NOSSA EMPRESA', '<p>\r\n	A Reisforts &eacute; uma empresa voltada para a fabrica&ccedil;&atilde;o e vendas de tendas e estrutura para eventos. Prima por uma atua&ccedil;&atilde;o inovadora e &eacute;tica, cuja qualidade &eacute; atestada por mais de 10 anos de mercado e uma clientela de renome como a Prefeitura de Goi&acirc;nia, Prefeitura de Palmeiras, Ilha Pura, Serra Grande, ADS Construtora (Mato Grosso), Tribunal da Justi&ccedil;a de Goi&aacute;s, Supermercado Bretas, entre outros.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Possui como objetivo, cativar seus clientes atrav&eacute;s de um atendimento &aacute;gil, personalizado e respeitoso, fornecendo-lhes produtos de &oacute;tima qualidade e na exata propor&ccedil;&atilde;o de suas necessidades.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>VENDAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A REISFORTS disp&otilde;e de ampla estrutura voltada para a produ&ccedil;&atilde;o de tendas e coberturas em diversos tamanhos e modelos. Nosso principal objetivo &eacute; atender nosso cliente com presteza e qualidade oriundas de comprovado Know How e do emprego de tecnologia de ponta.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nossas tendas s&atilde;o fabricadas por profissionais especializados e formados dentro de nossa escola de costura e soldagem eletr&ocirc;nica. Tudo isso para garantir que o nosso padr&atilde;o de excel&ecirc;ncia se manifeste em cada produto e surpreenda nosso cliente positivamente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Agora dispomos tamb&eacute;m de Banheiros Qu&iacute;micos para venda nos modelos: Banheiro Qu&iacute;mico para Banho, Banheiro Standard, VIP e PNE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>LOCA&Ccedil;&Atilde;O</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A REISFORTS disp&otilde;e de ampla estrutura voltada para a presta&ccedil;&atilde;o de servi&ccedil;os no ramo de loca&ccedil;&atilde;o de tendas e banheiros qu&iacute;micos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Dispomos de mais de 200 banheiros qu&iacute;micos de &uacute;ltima gera&ccedil;&atilde;o fabricados com tecnologia americana que, al&eacute;m da t&iacute;pica comodidade, ofereceram seguran&ccedil;a, beleza e praticidade para seu evento.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por sermos 100% a favor de inclus&atilde;o social e apostarmos nas tecnologias mais atuais, &eacute; que adquirimos banheiros qu&iacute;micos port&aacute;teis americanos desenvolvidos especificamente para uso de portadores de necessidades especiais.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Se n&atilde;o bastasse isso, a ReisForts traz para o mercado goiano a &uacute;ltima palavra em termos de saneamento m&oacute;vel: o banheiro qu&iacute;mico vip (ou banheiro qu&iacute;mico de luxo). Sua praticidade extrapola a do sanit&aacute;rio qu&iacute;mico port&aacute;til tradicional, porquanto al&eacute;m das qualidades deste, apresenta pia, porta papel toalha, porta sabonete l&iacute;quido e mecanismo de descarga.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	No que tange &agrave;s tendas, gostamos de frisar que somos os pr&oacute;prios fabricantes dos produtos utilizados nas loca&ccedil;&otilde;es, sendo esta uma garantia de que todos os produtos utilizados na presta&ccedil;&atilde;o de servi&ccedil;o passam por rigoroso processo de controle de qualidade que prima pela seguran&ccedil;a, adequa&ccedil;&atilde;o est&eacute;tica e funcionalidade de todas as nossas tendas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Nossa equipe de montadores &eacute; amplamente capacitada e treinada para atender &agrave;s mais diversas necessidades de nossos clientes.</p>\r\n<p>\r\n	&nbsp;</p>', 'SIM', 0, '', '', '', 'nossa-empresa', NULL, NULL, NULL),
(2, 'REISFORTS', '<div>\r\n	A Reisforts &eacute; uma empresa voltada para a fabrica&ccedil;&atilde;o e vendas de tendas e estrutura para eventos. Prima por uma atua&ccedil;&atilde;o inovadora e &eacute;tica, cuja qualidade &eacute; atestada por mais de 10 anos de mercado e uma clientela de renome como a Prefeitura de Goi&acirc;nia, Prefeitura de Palmeiras, Ilha Pura, Serra Grande, ADS Construtora (Mato Grosso), Tribunal da Justi&ccedil;a de Goi&aacute;s, Supermercado Bretas, entre outros.</div>', 'SIM', 0, '', '', '', 'reisforts', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_equipes`
--

CREATE TABLE IF NOT EXISTS `tb_equipes` (
  `idequipe` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idequipe`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_especificacoes`
--

CREATE TABLE IF NOT EXISTS `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext,
  PRIMARY KEY (`idespecificacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=6 ;

--
-- Fazendo dump de dados para tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_facebook`
--

CREATE TABLE IF NOT EXISTS `tb_facebook` (
  `idface` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idface`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=44 ;

--
-- Fazendo dump de dados para tabela `tb_facebook`
--

INSERT INTO `tb_facebook` (`idface`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'face 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'face-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'face 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'face 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'face 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'face 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'face 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'face 7 Lorem ipsum dolor sit amet', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias`
--

CREATE TABLE IF NOT EXISTS `tb_galerias` (
  `idgaleria` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=39 ;

--
-- Fazendo dump de dados para tabela `tb_galerias`
--

INSERT INTO `tb_galerias` (`idgaleria`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(36, 'GALERIA INDUSTRIAL', '2409201612211126493097.png', 'SIM', NULL, 'galeria-industrial', NULL),
(37, 'GALERIA DE ROUPAS', '2409201612231228099970.png', 'SIM', NULL, 'galeria-de-roupas', NULL),
(38, 'GALERIA COMÉRCIO GERAL', '1509201603311168460558.jpg', 'SIM', NULL, 'galeria-comercio-geral', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_geral`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_geral` (
  `id_galeriageral` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriageral`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Fazendo dump de dados para tabela `tb_galerias_geral`
--

INSERT INTO `tb_galerias_geral` (`id_galeriageral`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(106, '2309201602251388789729.jpg', 'SIM', NULL, NULL, 36),
(107, '2309201602251277639821.jpg', 'SIM', NULL, NULL, 36),
(108, '2309201602251115800518.jpg', 'SIM', NULL, NULL, 36),
(110, '2309201606301136720385.jpg', 'SIM', NULL, NULL, 37);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaportifolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=36 ;

--
-- Fazendo dump de dados para tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=296 ;

--
-- Fazendo dump de dados para tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(110, '1010201611417211867794.jpg', 'SIM', NULL, NULL, 9),
(111, '1010201611414734250112.jpg', 'SIM', NULL, NULL, 9),
(112, '1010201611416365632733.jpg', 'SIM', NULL, NULL, 9),
(113, '1010201611457285585436.jpg', 'SIM', NULL, NULL, 10),
(114, '1010201611453536913734.jpg', 'SIM', NULL, NULL, 10),
(115, '1010201611451350861936.jpg', 'SIM', NULL, NULL, 10),
(116, '1010201611452112572706.jpg', 'SIM', NULL, NULL, 10),
(117, '1010201611495251828754.jpg', 'SIM', NULL, NULL, 11),
(118, '1010201611493620939494.jpg', 'SIM', NULL, NULL, 11),
(119, '1010201611493291821914.jpg', 'SIM', NULL, NULL, 11),
(120, '1010201611497482193956.jpg', 'SIM', NULL, NULL, 11),
(121, '1010201611491978567764.jpg', 'SIM', NULL, NULL, 11),
(122, '1010201611497285552811.jpg', 'SIM', NULL, NULL, 11),
(123, '1010201611498613881224.jpg', 'SIM', NULL, NULL, 11),
(124, '1010201611498069978708.jpg', 'SIM', NULL, NULL, 11),
(125, '1010201611526618442291.jpg', 'SIM', NULL, NULL, 12),
(126, '1010201611526474658519.jpg', 'SIM', NULL, NULL, 12),
(127, '1010201611526748160810.jpg', 'SIM', NULL, NULL, 12),
(128, '1010201611524102561838.jpg', 'SIM', NULL, NULL, 12),
(129, '1010201611526734003321.jpg', 'SIM', NULL, NULL, 12),
(130, '1010201611537740244020.jpg', 'SIM', NULL, NULL, 12),
(131, '1010201611569597523411.jpg', 'SIM', NULL, NULL, 13),
(132, '1010201611563684668557.jpg', 'SIM', NULL, NULL, 13),
(133, '1010201611565358292646.jpg', 'SIM', NULL, NULL, 13),
(134, '1010201611568502312592.jpg', 'SIM', NULL, NULL, 13),
(135, '1010201611566464522236.jpg', 'SIM', NULL, NULL, 13),
(136, '1010201611566064865568.jpg', 'SIM', NULL, NULL, 13),
(137, '1010201611567769812556.jpg', 'SIM', NULL, NULL, 13),
(138, '1110201612003146461997.jpg', 'SIM', NULL, NULL, 14),
(139, '1110201612008731493449.jpg', 'SIM', NULL, NULL, 14),
(140, '1110201612005847989080.jpg', 'SIM', NULL, NULL, 14),
(141, '1110201612001853910001.jpg', 'SIM', NULL, NULL, 14),
(142, '1110201612007233389255.jpg', 'SIM', NULL, NULL, 14),
(143, '1110201612006667469603.jpg', 'SIM', NULL, NULL, 14),
(144, '1110201612002757961141.jpg', 'SIM', NULL, NULL, 14),
(145, '1110201612002909533248.jpg', 'SIM', NULL, NULL, 14),
(146, '1110201612009573349849.jpg', 'SIM', NULL, NULL, 14),
(147, '1110201612004879735203.jpg', 'SIM', NULL, NULL, 14),
(148, '1110201612008715124448.jpg', 'SIM', NULL, NULL, 14),
(149, '1110201612001153845451.jpg', 'SIM', NULL, NULL, 14),
(150, '1110201612005958763472.jpg', 'SIM', NULL, NULL, 14),
(151, '1110201612024585821214.jpg', 'SIM', NULL, NULL, 15),
(152, '1110201612029281335510.jpg', 'SIM', NULL, NULL, 15),
(153, '1110201612025527057225.jpg', 'SIM', NULL, NULL, 15),
(154, '1110201612026045235490.jpg', 'SIM', NULL, NULL, 15),
(155, '1110201612025481250455.jpg', 'SIM', NULL, NULL, 15),
(156, '1110201612025272281070.jpg', 'SIM', NULL, NULL, 15),
(157, '1110201612022782732045.jpg', 'SIM', NULL, NULL, 15),
(158, '1110201612038378070439.jpg', 'SIM', NULL, NULL, 15),
(159, '1110201612038849582543.jpg', 'SIM', NULL, NULL, 15),
(160, '1110201612039285688652.jpg', 'SIM', NULL, NULL, 15),
(171, '1110201612064104567923.jpg', 'SIM', NULL, NULL, 16),
(172, '1110201612062830996860.jpg', 'SIM', NULL, NULL, 16),
(173, '1110201612064872946027.jpg', 'SIM', NULL, NULL, 16),
(174, '1110201612061212010776.jpg', 'SIM', NULL, NULL, 16),
(175, '1110201612081699064493.jpg', 'SIM', NULL, NULL, 16),
(176, '1110201612082161941370.jpg', 'SIM', NULL, NULL, 16),
(177, '1110201612084761037251.jpg', 'SIM', NULL, NULL, 16),
(178, '1110201612088752837152.jpg', 'SIM', NULL, NULL, 16),
(179, '1110201612086768201279.jpg', 'SIM', NULL, NULL, 16),
(180, '1110201612085012633920.jpg', 'SIM', NULL, NULL, 16),
(181, '1110201612085496183757.jpg', 'SIM', NULL, NULL, 16),
(182, '1110201612082864888604.jpg', 'SIM', NULL, NULL, 16),
(183, '1110201612087104357489.jpg', 'SIM', NULL, NULL, 16),
(191, '1110201612309688340177.jpg', 'SIM', NULL, NULL, 18),
(192, '1110201612312967736716.jpg', 'SIM', NULL, NULL, 18),
(193, '1110201612318743844388.jpg', 'SIM', NULL, NULL, 18),
(194, '1110201612315998348970.jpg', 'SIM', NULL, NULL, 18),
(195, '1110201612311450883775.jpg', 'SIM', NULL, NULL, 18),
(196, '1110201612312843069376.jpg', 'SIM', NULL, NULL, 18),
(197, '1110201612313243602423.jpg', 'SIM', NULL, NULL, 18),
(198, '1110201612311733590745.jpg', 'SIM', NULL, NULL, 18),
(199, '1110201612315398214620.jpg', 'SIM', NULL, NULL, 18),
(200, '1110201612313801929350.jpg', 'SIM', NULL, NULL, 18),
(201, '1110201612316787839461.jpg', 'SIM', NULL, NULL, 18),
(202, '1110201612312391056099.jpg', 'SIM', NULL, NULL, 18),
(203, '1110201612317700411929.jpg', 'SIM', NULL, NULL, 18),
(204, '1110201612313070146267.jpg', 'SIM', NULL, NULL, 18),
(205, '1110201612319329680452.jpg', 'SIM', NULL, NULL, 18),
(206, '1110201612318310491931.jpg', 'SIM', NULL, NULL, 18),
(207, '1110201612316271296863.jpg', 'SIM', NULL, NULL, 18),
(208, '1110201612358112008505.jpg', 'SIM', NULL, NULL, 19),
(209, '1110201612351397311157.jpg', 'SIM', NULL, NULL, 19),
(210, '1110201612353003591241.jpg', 'SIM', NULL, NULL, 19),
(211, '1110201612359886949343.jpg', 'SIM', NULL, NULL, 19),
(212, '1110201612396954350396.jpg', 'SIM', NULL, NULL, 20),
(213, '1110201612399615893483.jpg', 'SIM', NULL, NULL, 20),
(214, '1110201612391864220425.jpg', 'SIM', NULL, NULL, 20),
(215, '1110201612397052185092.jpg', 'SIM', NULL, NULL, 20),
(216, '1110201612396673967676.jpg', 'SIM', NULL, NULL, 20),
(217, '1110201612417519827079.jpg', 'SIM', NULL, NULL, 21),
(218, '1110201612418105266201.jpg', 'SIM', NULL, NULL, 21),
(219, '1110201612413477423712.jpg', 'SIM', NULL, NULL, 21),
(220, '1110201612415553685712.jpg', 'SIM', NULL, NULL, 21),
(221, '1110201612446084257480.jpg', 'SIM', NULL, NULL, 22),
(222, '1110201612448762485840.jpg', 'SIM', NULL, NULL, 22),
(223, '1110201612441636207761.jpg', 'SIM', NULL, NULL, 22),
(224, '1110201612445694692908.jpg', 'SIM', NULL, NULL, 22),
(225, '1110201612452044791922.jpg', 'SIM', NULL, NULL, 22),
(227, '1110201612528433145102.jpg', 'SIM', NULL, NULL, 23),
(228, '1110201612531225798216.jpg', 'SIM', NULL, NULL, 23),
(229, '1110201612539434430243.jpg', 'SIM', NULL, NULL, 23),
(230, '1110201612536572742722.jpg', 'SIM', NULL, NULL, 23),
(231, '1110201612535417358076.jpg', 'SIM', NULL, NULL, 23),
(233, '1110201612562087603641.jpg', 'SIM', NULL, NULL, 24),
(234, '1110201612563412636576.jpg', 'SIM', NULL, NULL, 24),
(235, '1110201612562019619747.jpg', 'SIM', NULL, NULL, 24),
(236, '1110201612561293092705.jpg', 'SIM', NULL, NULL, 24),
(237, '1110201612579691424423.jpg', 'SIM', NULL, NULL, 24),
(240, '1110201601072267276673.jpg', 'SIM', NULL, NULL, 25),
(241, '1110201601078217438124.jpg', 'SIM', NULL, NULL, 25),
(242, '1110201601086412798542.jpg', 'SIM', NULL, NULL, 25),
(243, '1110201601081341464051.jpg', 'SIM', NULL, NULL, 25),
(245, '1110201601148371009209.jpg', 'SIM', NULL, NULL, 26),
(250, '1110201601153356862390.jpg', 'SIM', NULL, NULL, 26),
(251, '1110201601161186090347.jpg', 'SIM', NULL, NULL, 26),
(252, '1110201601173025524578.jpg', 'SIM', NULL, NULL, 26),
(253, '1110201601174859511620.jpg', 'SIM', NULL, NULL, 26),
(254, '1110201601175414430962.jpg', 'SIM', NULL, NULL, 26),
(255, '1110201601176223410173.jpg', 'SIM', NULL, NULL, 26),
(256, '0912201612109978871357.jpg', 'SIM', NULL, NULL, 17),
(257, '0912201612108696928951.jpg', 'SIM', NULL, NULL, 17),
(258, '0912201612103958997879.jpg', 'SIM', NULL, NULL, 17),
(259, '0912201612104827032333.jpg', 'SIM', NULL, NULL, 17),
(260, '1306201705366442446394.jpg', 'SIM', NULL, NULL, 13),
(261, '1306201705371902206035.jpg', 'SIM', NULL, NULL, 13),
(262, '1306201705376317498716.jpg', 'SIM', NULL, NULL, 13),
(263, '1306201705371644171703.jpg', 'SIM', NULL, NULL, 13),
(264, '1306201705373152953419.jpg', 'SIM', NULL, NULL, 13),
(265, '1306201705379627435368.jpg', 'SIM', NULL, NULL, 13),
(266, '1306201705376116138141.jpg', 'SIM', NULL, NULL, 13),
(267, '1306201705406280311035.jpg', 'SIM', NULL, NULL, 18),
(268, '1306201705409389043218.jpg', 'SIM', NULL, NULL, 18),
(269, '1306201705408732998495.jpg', 'SIM', NULL, NULL, 18),
(270, '1306201705404167557424.jpg', 'SIM', NULL, NULL, 18),
(271, '1306201705405257453040.jpg', 'SIM', NULL, NULL, 18),
(272, '1306201705407810357759.jpg', 'SIM', NULL, NULL, 18),
(275, '1306201705403029480940.jpg', 'SIM', NULL, NULL, 18),
(277, '1306201705412884075024.jpg', 'SIM', NULL, NULL, 18),
(278, '1306201705445331422007.jpg', 'SIM', NULL, NULL, 15),
(279, '1306201705448804835366.jpg', 'SIM', NULL, NULL, 15),
(280, '1306201705442934920473.jpg', 'SIM', NULL, NULL, 15),
(281, '1306201705448643463576.jpg', 'SIM', NULL, NULL, 15),
(282, '1306201705442618388214.jpg', 'SIM', NULL, NULL, 15),
(283, '1306201705442130989394.jpg', 'SIM', NULL, NULL, 15),
(285, '1306201705443502386899.jpg', 'SIM', NULL, NULL, 15),
(286, '1306201705448605961091.jpg', 'SIM', NULL, NULL, 15),
(287, '1306201705446658685000.jpg', 'SIM', NULL, NULL, 15),
(289, '1306201705441842409835.jpg', 'SIM', NULL, NULL, 15),
(290, '1306201705445641406306.jpg', 'SIM', NULL, NULL, 15),
(291, '1306201705448362175631.jpg', 'SIM', NULL, NULL, 15),
(292, '1306201705523786050561.jpg', 'SIM', NULL, NULL, 16),
(293, '1306201705523314262934.jpg', 'SIM', NULL, NULL, 16),
(294, '1306201705521212490027.jpg', 'SIM', NULL, NULL, 16),
(295, '1306201705522278990633.jpg', 'SIM', NULL, NULL, 16);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_servicos` (
  `id_galeriaservico` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaservico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galeria_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaempresa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=23 ;

--
-- Fazendo dump de dados para tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(1, 'EMPRESA GALERIA 01', '1509201611341137587104.jpg', 'SIM', NULL, 'empresa-galeria-01', NULL),
(2, 'EMPRESA GALERIA 02', '1509201611341137587104.jpg', 'SIM', NULL, NULL, NULL),
(21, 'EMPRESA GALERIA 03', '1509201611341137587104.jpg', 'SIM', NULL, NULL, NULL),
(22, 'EMPRESA GALERIA 04', '1509201611521224468872.jpg', 'SIM', NULL, 'empresa-galeria-04', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO',
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=5 ;

--
-- Fazendo dump de dados para tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL, 'NAO'),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre', 'SIM'),
(3, 'Amanda', 'b362cb319b2e525dc715702edf416f10', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', NULL, 'SIM'),
(4, 'Renato ReisForts', '2e50632e45139aa91f8a0d75a907b137', 'SIM', 0, 'renato@reisforts.com.br', 'NAO', 'renato-reisforts', 'NAO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1240 ;

--
-- Fazendo dump de dados para tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''4''', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''6''', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''6''', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''43''', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''43''', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''44''', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''44''', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''46''', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''45''', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''46''', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''45''', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''3''', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''NAO'' WHERE idlogin = ''2''', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''SIM'' WHERE idlogin = ''2''', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''4''', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''5''', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''6''', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = '''' WHERE idlogin = ''2''', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = ''2'', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''4''', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''5''', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = ''1''', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''70''', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''73''', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''71''', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''72''', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''81''', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''SIM'' WHERE idcategoriaproduto = ''81''', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = ''NAO'' WHERE idnoticia = ''46''', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = ''NAO'' WHERE idnoticia = ''45''', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = ''45''', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = ''SIM'' WHERE idnoticia = ''46''', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''80''', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''81''', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''82''', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''80''', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''81''', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''82''', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''2''', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''5''', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''40''', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''42''', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''43''', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''44''', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''8''', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''9''', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''10''', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''11''', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''12''', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''13''', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''14''', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''41''', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''46''', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1),
(489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:35:23', 1),
(490, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:48:13', 1),
(491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:16', 1),
(492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:59', 1),
(493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:27:37', 1),
(494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:28:23', 1),
(495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:33:30', 1),
(496, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:20', 1),
(497, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:27', 1),
(498, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:04:09', 1),
(499, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '13:12:53', 1),
(500, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:08:56', 1),
(501, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:10:32', 1),
(502, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:19:38', 1),
(503, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''5''', '2016-06-07', '14:20:12', 1),
(504, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''3''', '2016-06-07', '14:20:29', 1),
(505, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''4''', '2016-06-07', '14:20:31', 1),
(506, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:25:43', 1),
(507, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:26:57', 1),
(508, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:31:49', 1),
(509, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:58:26', 1),
(510, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:01:49', 1),
(511, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-06-07', '15:02:31', 1),
(512, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:02:44', 1),
(513, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:06:22', 1),
(514, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-06-07', '15:06:29', 1),
(515, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-06-07', '15:07:17', 1),
(516, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:08:05', 1),
(517, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''2''', '2016-06-07', '19:19:12', 1),
(518, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '19:20:53', 1),
(519, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-06-07', '16:48:33', 1),
(520, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '16:57:35', 1),
(521, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:13', 1),
(522, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:59', 1),
(523, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:05:10', 3),
(524, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:08:08', 3),
(525, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '12:30:55', 1),
(526, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '15:38:33', 1),
(527, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '18:39:40', 3),
(528, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '20:42:20', 3),
(529, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:40:49', 1),
(530, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:41:59', 1),
(531, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:43:56', 1),
(532, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:47:20', 1),
(533, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:07:52', 1),
(534, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:09:32', 1),
(535, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:10:41', 1),
(536, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:05', 1),
(537, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:39', 1),
(538, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:15:18', 1),
(539, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:16:56', 1),
(540, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:17:48', 1),
(541, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:54:16', 1),
(542, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:55:13', 1),
(543, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:37:40', 1),
(544, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:43:05', 1),
(545, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:46:24', 1),
(546, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '09:11:11', 1),
(547, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '11:34:48', 1),
(548, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '12:03:24', 1),
(549, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:02:56', 0),
(550, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:39:29', 0),
(551, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:40:31', 0),
(552, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:05:34', 0),
(553, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:13:26', 0),
(554, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:15:05', 0),
(555, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '21:31:04', 0),
(556, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:12:42', 1),
(557, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:23', 1),
(558, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:54', 1),
(559, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:14:21', 1),
(560, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:09:27', 0),
(561, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:10:20', 0),
(562, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:15:20', 0),
(563, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '01:11:29', 0),
(564, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:35:50', 1),
(565, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:55:00', 0),
(566, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:55:01', 1),
(567, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:59:56', 0),
(568, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:49:36', 1),
(569, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:50:07', 1),
(570, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '11:03:55', 1),
(571, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:52:10', 1),
(572, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '13:19:01', 1),
(573, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:00:30', 1),
(574, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:41:42', 1),
(575, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:30:13', 1),
(576, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:32:16', 1),
(577, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-06', '17:07:15', 1),
(578, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:50:58', 1),
(579, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:10', 1),
(580, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:22', 1),
(581, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:30', 1),
(582, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '13:56:40', 1),
(583, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '14:02:37', 1),
(584, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '17:20:46', 1),
(585, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-13', '21:33:07', 1),
(586, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '10:53:24', 1),
(587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:37:57', 1),
(588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:42:35', 1),
(589, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:46:29', 1),
(590, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:51:04', 1),
(591, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:24:55', 1),
(592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:27:05', 1),
(593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:28:29', 1),
(594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:02', 1),
(595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:29', 1),
(596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:39', 1),
(597, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:48', 1),
(598, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:40:05', 1),
(599, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '15:47:41', 1),
(600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:53:52', 1),
(601, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:42', 1),
(602, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:59', 1),
(603, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:13', 1),
(604, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:26', 1),
(605, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:41', 1),
(606, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:53', 1),
(607, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:59:08', 1),
(608, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '16:32:46', 1),
(609, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '18:56:30', 1),
(610, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:12', 1),
(611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:31', 1),
(612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:48', 1),
(613, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:00', 1),
(614, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:22', 1),
(615, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:46', 1),
(616, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:15:06', 1),
(617, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '19:17:31', 1),
(618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:30:31', 1),
(619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '08:52:29', 1),
(620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:58:06', 1),
(621, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '19:46:29', 1),
(622, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '19:47:35', 1),
(623, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''14''', '2016-07-15', '19:59:25', 1),
(624, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''13''', '2016-07-15', '19:59:32', 1),
(625, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''15''', '2016-07-15', '19:59:36', 1),
(626, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''16''', '2016-07-15', '19:59:42', 1),
(627, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''18''', '2016-07-15', '19:59:45', 1),
(628, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''17''', '2016-07-15', '19:59:49', 1),
(629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '20:14:55', 1),
(630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '11:00:46', 1),
(631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:28:19', 1),
(632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:37:56', 1),
(633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:38:07', 1),
(634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:05', 1),
(635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:53', 1),
(636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:11', 1),
(637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:27', 1),
(638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:45', 1),
(639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:06', 1),
(640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:29', 1),
(641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:58', 1),
(642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:02:46', 1),
(643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:04:37', 1),
(644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:12:13', 1),
(645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:15:36', 1),
(646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:06:46', 1),
(647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:49:01', 1),
(648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:54:14', 1),
(649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:55:10', 1),
(650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:00:07', 1),
(651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:08:04', 1),
(652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:15:21', 1),
(653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:16:03', 1),
(654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:19:17', 1),
(655, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:22:55', 1),
(656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:25:29', 1),
(657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '09:35:26', 1),
(658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '18:33:00', 1),
(659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '08:47:49', 1),
(660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:01:41', 1),
(661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:30:24', 1),
(662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:46:31', 1),
(663, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:41:53', 1),
(664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:42:27', 1),
(665, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:01:08', 1),
(666, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:59:31', 1),
(667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '13:07:58', 1),
(668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '19:12:55', 1),
(669, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_facebook WHERE idface = ''43''', '2016-07-21', '19:23:48', 1),
(670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:05', 1),
(671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:28', 1),
(672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:48', 1),
(673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:08', 1),
(674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:26', 1),
(675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:41', 1),
(676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:28', 1),
(677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:36', 1),
(678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:46', 1),
(679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:57', 1),
(680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:34:45', 1),
(681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:57:51', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:59:26', 1),
(683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:01:05', 1),
(684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:38', 1),
(685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:55', 1),
(686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:09', 1),
(687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:26', 1),
(688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:45', 1),
(689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:06:02', 1),
(690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '18:07:45', 1),
(691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:29:36', 1),
(692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:32:50', 1),
(693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:33:06', 1),
(694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:14', 1),
(695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:39', 1),
(696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-29', '08:57:37', 1),
(697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '12:23:41', 1),
(698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '15:32:54', 1),
(699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:54:41', 1),
(700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:55:28', 1),
(701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '18:42:16', 1),
(702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '19:42:53', 1),
(703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:04:57', 1),
(704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:12:02', 1),
(705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-31', '14:29:17', 1),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:28', 1),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:42', 1),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:56', 1),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:52:43', 1),
(710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:15:31', 1),
(711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:21:54', 1),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '12:04:07', 1),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '15:58:27', 1),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '16:02:46', 1),
(715, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '17:35:49', 1),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:56:30', 1),
(717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:01', 1),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:12', 1),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:23', 1),
(720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '16:28:16', 1),
(721, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '17:20:26', 1),
(722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '18:37:39', 1),
(723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:25:58', 1),
(724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:26:08', 1),
(725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:36', 1),
(726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:47', 1),
(727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '09:09:03', 1),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '10:12:40', 1),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:44:59', 1),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:52:14', 1),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:12:56', 1),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:47:19', 1),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-04', '20:15:30', 1),
(734, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:29', 1),
(735, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:38', 1),
(736, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: teste1@gmail.com', 'DELETE FROM tb_equipes WHERE idequipe = ''1''', '2016-08-25', '11:30:04', 1),
(737, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''2''', '2016-08-25', '11:30:06', 1),
(738, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''3''', '2016-08-25', '11:30:08', 1),
(739, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''4''', '2016-08-25', '11:30:09', 1),
(740, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''5''', '2016-08-25', '11:30:11', 1),
(741, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''6''', '2016-08-25', '11:30:13', 1),
(742, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''7''', '2016-08-25', '11:30:15', 1),
(743, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''8''', '2016-08-25', '11:30:19', 1),
(744, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''9''', '2016-08-25', '11:30:21', 1),
(745, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''10''', '2016-08-25', '11:30:23', 1),
(746, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''11''', '2016-08-25', '11:30:25', 1),
(747, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''12''', '2016-08-25', '11:30:27', 1),
(748, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:12', 1),
(749, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:41', 1),
(750, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:55', 1),
(751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:02', 1),
(752, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:09', 1),
(753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:15', 1),
(754, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:22', 1),
(755, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:47', 1),
(756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:56', 1),
(757, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:05', 1),
(758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:37', 1),
(759, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:08', 1),
(760, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:30', 1),
(761, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''1''', '2016-08-25', '11:36:53', 1),
(762, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''2''', '2016-08-25', '11:36:54', 1),
(763, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''3''', '2016-08-25', '11:36:56', 1),
(764, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''4''', '2016-08-25', '11:36:58', 1),
(765, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''5''', '2016-08-25', '11:37:01', 1),
(766, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''6''', '2016-08-25', '11:37:03', 1),
(767, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''7''', '2016-08-25', '11:37:05', 1),
(768, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''8''', '2016-08-25', '11:37:07', 1),
(769, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''9''', '2016-08-25', '11:37:09', 1),
(770, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''10''', '2016-08-25', '11:37:10', 1),
(771, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''11''', '2016-08-25', '11:37:12', 1),
(772, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''12''', '2016-08-25', '11:37:13', 1),
(773, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''13''', '2016-08-25', '11:37:15', 1),
(774, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''14''', '2016-08-25', '11:37:17', 1),
(775, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''15''', '2016-08-25', '11:37:19', 1),
(776, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''16''', '2016-08-25', '11:37:21', 1),
(777, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''17''', '2016-08-25', '11:37:22', 1),
(778, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''18''', '2016-08-25', '11:37:24', 1),
(779, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-25', '11:38:15', 1),
(780, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-25', '11:38:17', 1),
(781, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-08-25', '11:38:19', 1),
(782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:39:33', 1),
(783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:41:06', 1),
(784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:48:51', 1),
(785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:49:46', 1),
(786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:51:10', 1),
(787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:18', 1),
(788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:50', 1),
(789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:02:47', 1),
(790, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''39''', '2016-08-25', '12:02:52', 1),
(791, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''40''', '2016-08-25', '12:02:55', 1),
(792, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''41''', '2016-08-25', '12:02:57', 1),
(793, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''42''', '2016-08-25', '12:02:59', 1),
(794, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''43''', '2016-08-25', '12:03:01', 1),
(795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:03:41', 1),
(796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:14:52', 1),
(797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:15:10', 1),
(798, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-08-25', '14:21:41', 1),
(799, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-08-25', '14:21:45', 1),
(800, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-08-25', '14:21:48', 1),
(801, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-08-25', '14:21:50', 1),
(802, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-08-25', '14:21:52', 1),
(803, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-08-25', '14:21:54', 1),
(804, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-08-25', '14:21:56', 1),
(805, 'CADASTRO DO CLIENTE ', '', '2016-08-25', '14:22:57', 1),
(806, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''8''', '2016-08-25', '14:23:51', 1),
(807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:32', 1),
(808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:55', 1),
(809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:17', 1),
(810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:30', 1),
(811, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''8''', '2016-08-25', '14:45:02', 1),
(812, 'EXCLUSÃO DO LOGIN 50, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''50''', '2016-08-25', '14:45:07', 1),
(813, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''51''', '2016-08-25', '14:45:11', 1),
(814, 'EXCLUSÃO DO LOGIN 52, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''52''', '2016-08-25', '14:45:15', 1),
(815, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''53''', '2016-08-25', '14:45:18', 1),
(816, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''54''', '2016-08-25', '14:45:20', 1),
(817, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''55''', '2016-08-25', '14:45:25', 1),
(818, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:01', 1),
(819, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:21', 1),
(820, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:47', 1),
(821, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:52:26', 1),
(822, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:55:43', 1),
(823, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:57:09', 1),
(824, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:59:15', 1),
(825, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:19', 1),
(826, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:59', 1),
(827, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:10:30', 1),
(828, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:12:32', 1),
(829, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:14', 1),
(830, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:41', 1),
(831, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:20:23', 1),
(832, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:21:01', 1),
(833, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:41:28', 1),
(834, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:47', 1),
(835, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:58', 1),
(836, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:09', 1),
(837, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:17', 1),
(838, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:24', 1),
(839, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:32', 1),
(840, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:40', 1),
(841, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:49', 1),
(842, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:56', 1),
(843, 'EXCLUSÃO DO LOGIN 151, NOME: , Email: ', 'DELETE FROM tb_atuacoes WHERE idatuacao = ''151''', '2016-08-26', '15:34:14', 1),
(844, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:36:16', 1),
(845, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:42:46', 1),
(846, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:05', 1),
(847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:22', 1),
(848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:33', 1),
(849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:46', 1),
(850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:16', 1),
(851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:52', 1),
(852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:45:04', 1),
(853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:46:30', 1),
(854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '18:51:26', 0),
(855, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '18:51:30', 0),
(856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:16:59', 0),
(857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:18:46', 0),
(858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:21:34', 0),
(859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:24:24', 0),
(860, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '19:24:28', 0),
(861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:33:13', 0),
(862, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-08-27', '19:33:40', 0),
(863, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '19:41:11', 0),
(864, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-27', '19:41:14', 0),
(865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:42:48', 0),
(866, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:43:20', 0),
(867, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '19:43:24', 0),
(868, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-08-27', '19:43:27', 0),
(869, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:47:39', 0),
(870, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '19:47:43', 0),
(871, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:50:56', 0),
(872, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:54:50', 0),
(873, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-08-27', '19:54:56', 0),
(874, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-27', '19:54:58', 0),
(875, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '19:55:00', 0),
(876, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:56:22', 0),
(877, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:19:06', 0),
(878, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '20:19:10', 0),
(879, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:20:55', 0),
(880, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:22:38', 0),
(881, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '20:22:41', 0),
(882, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:29:14', 0),
(883, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:32:24', 0),
(884, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '20:32:43', 0),
(885, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:34:14', 0),
(886, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:47:12', 0),
(887, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '20:47:22', 0),
(888, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:49:08', 0),
(889, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:53:49', 0),
(890, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:59:22', 0),
(891, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:01:55', 0),
(892, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:05:47', 0),
(893, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:27:16', 0),
(894, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:46:33', 0),
(895, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '11:46:28', 1),
(896, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''77''', '2016-08-29', '11:46:37', 1),
(897, 'EXCLUSÃO DO LOGIN 78, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''78''', '2016-08-29', '11:46:39', 1),
(898, 'EXCLUSÃO DO LOGIN 79, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''79''', '2016-08-29', '11:46:41', 1),
(899, 'CADASTRO DO CLIENTE ', '', '2016-08-29', '11:49:11', 1),
(900, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '16:39:22', 13),
(901, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:47:03', 13),
(902, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:25', 13),
(903, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:41', 13),
(904, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:52', 13),
(905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:26:42', 1),
(906, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:28:49', 1),
(907, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:35:08', 1),
(908, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:57:02', 1),
(909, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:27:02', 1),
(910, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:28:27', 1),
(911, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:31:51', 1),
(912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:35:11', 1),
(913, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:38:10', 1),
(914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:53:56', 1),
(915, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:01:02', 1),
(916, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:01:40', 1),
(917, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:03:58', 1),
(918, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:04:19', 1),
(919, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:05:43', 1),
(920, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:09:40', 1),
(921, 'EXCLUSÃO DO LOGIN 74, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''74''', '2016-09-13', '19:09:48', 1),
(922, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:26', 1),
(923, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:47', 1),
(924, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:15:07', 1),
(925, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:57:44', 1),
(926, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '20:01:10', 1),
(927, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:23:45', 1),
(928, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:24:20', 1),
(929, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:27:15', 1),
(930, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:33:52', 1),
(931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '10:39:26', 1),
(932, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:34:16', 1),
(933, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '11:46:08', 1),
(934, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:52:01', 1),
(935, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:07:45', 1),
(936, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:04', 1),
(937, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:42', 1),
(938, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:58', 1),
(939, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:11:57', 1),
(940, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:29:28', 1),
(941, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:29:48', 1),
(942, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:30:16', 1),
(943, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:31:19', 1),
(944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '17:12:33', 1),
(945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '19:50:15', 1),
(946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '08:37:22', 1),
(947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:01:49', 1),
(948, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:38:31', 1),
(949, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''37''', '2016-09-16', '09:38:38', 1),
(950, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''38''', '2016-09-16', '09:38:41', 1),
(951, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:41:49', 1),
(952, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:26', 1),
(953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:41', 1),
(954, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:07:02', 1),
(955, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:54:08', 1),
(956, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:02:52', 1),
(957, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:03:05', 1),
(958, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:33:32', 1),
(959, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '15:53:42', 1),
(960, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '19:20:41', 1),
(961, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '10:48:09', 1),
(962, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:49:25', 1),
(963, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:52:15', 1),
(964, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:08', 1),
(965, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:47', 1),
(966, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''1''', '2016-09-17', '14:48:21', 1),
(967, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''2''', '2016-09-17', '14:48:29', 1),
(968, 'DESATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''1''', '2016-09-17', '14:48:34', 1),
(969, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '14:50:07', 1),
(970, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''2''', '2016-09-17', '14:51:18', 1),
(971, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''1''', '2016-09-17', '14:51:21', 1),
(972, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:27:48', 1),
(973, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:28:34', 1),
(974, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '17:30:06', 1),
(975, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:13', 1),
(976, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:49', 1),
(977, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:11:03', 1),
(978, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:14:08', 1),
(979, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:11', 1),
(980, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:30', 1),
(981, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:48', 1),
(982, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:55:08', 1),
(983, 'CADASTRO DO CLIENTE ', '', '2016-09-18', '11:09:45', 1),
(984, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:43:17', 1),
(985, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:44:24', 1),
(986, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:51', 1),
(987, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:58', 1),
(988, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:07', 1),
(989, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:13', 1),
(990, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '15:06:09', 1),
(991, 'EXCLUSÃO DO LOGIN 85, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''85''', '2016-09-19', '11:05:44', 1),
(992, 'EXCLUSÃO DO LOGIN 76, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''76''', '2016-09-19', '11:05:46', 1),
(993, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''83''', '2016-09-19', '11:05:49', 1),
(994, 'EXCLUSÃO DO LOGIN 84, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''84''', '2016-09-19', '11:05:52', 1),
(995, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:07:45', 1),
(996, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:13:50', 1),
(997, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:14:46', 1),
(998, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:15:35', 1),
(999, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:17:05', 1),
(1000, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:21:57', 1),
(1001, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:03', 1),
(1002, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:09', 1),
(1003, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:15', 1),
(1004, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:20', 1),
(1005, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:27:28', 1),
(1006, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:28:25', 1),
(1007, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:06:56', 1),
(1008, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:07:31', 1),
(1009, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:10:36', 1),
(1010, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:26', 1),
(1011, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:53', 1),
(1012, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '13:55:28', 1),
(1013, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:36:48', 1),
(1014, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:08', 1),
(1015, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:15', 1),
(1016, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:01', 1),
(1017, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:07', 1),
(1018, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:44:41', 1),
(1019, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:17', 1),
(1020, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:45', 1),
(1021, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:25', 1),
(1022, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:35', 1),
(1023, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:52', 1),
(1024, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:02:04', 1),
(1025, 'CADASTRO DO CLIENTE ', '', '2016-09-21', '14:58:35', 1),
(1026, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '15:45:26', 1),
(1027, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:06:57', 1),
(1028, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:07:12', 1),
(1029, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:51:52', 1),
(1030, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '20:17:45', 1),
(1031, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '08:46:52', 1),
(1032, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '09:19:31', 1),
(1033, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '10:09:37', 1),
(1034, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '11:36:44', 1),
(1035, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:23', 1),
(1036, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:40', 1),
(1037, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:04', 1),
(1038, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''39''', '2016-09-23', '12:49:16', 1),
(1039, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:42', 1),
(1040, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:07:13', 1),
(1041, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:20:03', 1),
(1042, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''40''', '2016-09-23', '14:07:52', 1),
(1043, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''41''', '2016-09-23', '14:07:56', 1),
(1044, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '16:32:57', 1),
(1045, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:21:42', 1),
(1046, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:22:23', 1),
(1047, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:23:20', 1),
(1048, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '17:38:06', 1),
(1049, 'CADASTRO DO CLIENTE ', '', '2016-09-24', '17:39:30', 1),
(1050, 'CADASTRO DO CLIENTE ', '', '2016-09-24', '17:40:29', 1),
(1051, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '17:41:40', 1),
(1052, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''3''', '2016-09-28', '12:55:59', 1),
(1053, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''5''', '2016-09-28', '12:56:03', 1),
(1054, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''6''', '2016-09-28', '12:56:05', 1),
(1055, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''7''', '2016-09-28', '12:56:07', 1),
(1056, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''9''', '2016-09-28', '12:56:10', 1),
(1057, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''8''', '2016-09-28', '12:56:15', 1),
(1058, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''12''', '2016-09-28', '12:56:33', 1),
(1059, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''11''', '2016-09-28', '12:56:35', 1),
(1060, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''10''', '2016-09-28', '12:56:37', 1),
(1061, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''4''', '2016-09-28', '12:56:40', 1),
(1062, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '12:57:23', 1),
(1063, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''13''', '2016-09-28', '13:01:37', 1),
(1064, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '13:24:19', 1),
(1065, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '13:24:28', 1),
(1066, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '13:24:43', 1),
(1067, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '13:25:06', 1),
(1068, 'EXCLUSÃO DO LOGIN 88, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''88''', '2016-09-28', '17:41:44', 1),
(1069, 'EXCLUSÃO DO LOGIN 86, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''86''', '2016-09-28', '17:41:47', 1),
(1070, 'EXCLUSÃO DO LOGIN 87, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''87''', '2016-09-28', '17:41:50', 1),
(1071, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '17:57:12', 1),
(1072, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '17:57:54', 1),
(1073, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''4''', '2016-09-28', '17:59:57', 1),
(1074, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''7''', '2016-09-28', '18:00:00', 1),
(1075, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''9''', '2016-09-28', '18:00:01', 1),
(1076, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''8''', '2016-09-28', '18:00:02', 1),
(1077, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''10''', '2016-09-28', '18:00:04', 1),
(1078, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''5''', '2016-09-28', '18:00:05', 1),
(1079, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:01:15', 1),
(1080, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:01:24', 1),
(1081, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:01:39', 1),
(1082, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:01:58', 1),
(1083, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:02:19', 1),
(1084, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:02:40', 1),
(1085, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '18:03:06', 1),
(1086, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '18:03:18', 1),
(1087, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '18:03:34', 1),
(1088, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '18:03:48', 1),
(1089, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '18:04:01', 1),
(1090, 'CADASTRO DO CLIENTE ', '', '2016-09-28', '18:04:16', 1),
(1091, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:06:40', 1),
(1092, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-28', '18:07:14', 1),
(1093, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '08:33:23', 1),
(1094, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '09:35:59', 1),
(1095, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '10:53:30', 1),
(1096, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '13:20:10', 1),
(1097, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '13:20:31', 1),
(1098, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '14:27:00', 1),
(1099, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '14:29:16', 1),
(1100, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '14:31:57', 1),
(1101, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '15:43:59', 1),
(1102, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '15:54:00', 1),
(1103, 'CADASTRO DO CLIENTE ', '', '2016-09-29', '18:12:40', 1),
(1104, 'CADASTRO DO CLIENTE ', '', '2016-09-29', '18:22:25', 1),
(1105, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-29', '18:40:05', 1),
(1106, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '09:43:43', 1),
(1107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '11:13:24', 1),
(1108, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '13:27:10', 1),
(1109, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '16:30:33', 1),
(1110, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '17:14:07', 1),
(1111, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '17:14:28', 1),
(1112, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '17:14:50', 1),
(1113, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '17:15:11', 1),
(1114, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '17:18:03', 1),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-30', '19:20:49', 1),
(1116, 'CADASTRO DO CLIENTE ', '', '2016-10-04', '18:37:44', 1),
(1117, 'CADASTRO DO CLIENTE ', '', '2016-10-04', '18:38:05', 1),
(1118, 'CADASTRO DO CLIENTE ', '', '2016-10-04', '18:38:24', 1),
(1119, 'CADASTRO DO CLIENTE ', '', '2016-10-04', '19:38:03', 1),
(1120, 'CADASTRO DO CLIENTE ', '', '2016-10-05', '12:47:51', 1),
(1121, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '15:34:30', 1),
(1122, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '15:38:54', 1),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '16:07:39', 1),
(1124, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '16:46:37', 1),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '16:59:32', 1),
(1126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '16:59:43', 1),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '17:34:31', 1),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-05', '18:00:40', 1),
(1129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-06', '10:20:47', 1),
(1130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-06', '17:08:53', 1),
(1131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '13:51:41', 1),
(1132, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '15:52:20', 3),
(1133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '15:58:15', 3),
(1134, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '15:59:06', 3),
(1135, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '16:03:35', 3),
(1136, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:04:52', 3),
(1137, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:05:04', 3),
(1138, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:05:16', 3),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:05:48', 3),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:06:25', 3),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:06:42', 3),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:07:12', 3),
(1143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:07:34', 3),
(1144, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '16:08:07', 3),
(1145, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '16:08:19', 3),
(1146, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '16:08:30', 3),
(1147, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '16:08:58', 3),
(1148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:19:37', 3),
(1149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:21:02', 3),
(1150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:21:54', 3),
(1151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:23:25', 3),
(1152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:41:50', 1),
(1153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '16:41:58', 1),
(1154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '23:40:14', 3),
(1155, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '23:40:39', 3),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '23:40:53', 3),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '23:44:31', 3),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '23:48:04', 3),
(1159, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '23:51:55', 3),
(1160, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-10', '23:56:11', 3),
(1161, 'CADASTRO DO CLIENTE ', '', '2016-10-10', '23:58:48', 3),
(1162, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:01:40', 3),
(1163, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:05:07', 3),
(1164, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:12:15', 3),
(1165, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:29:45', 3),
(1166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '00:32:34', 3),
(1167, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:35:14', 3),
(1168, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:38:29', 3),
(1169, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:41:06', 3),
(1170, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:43:47', 3),
(1171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '00:50:27', 3),
(1172, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:52:19', 3),
(1173, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:54:24', 3),
(1174, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:54:34', 3),
(1175, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '00:55:54', 3),
(1176, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:05:16', 3),
(1177, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:05:30', 3),
(1178, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:06:58', 3),
(1179, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:11:47', 3),
(1180, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:12:03', 3),
(1181, 'EXCLUSÃO DO LOGIN 94, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''94''', '2016-10-11', '01:12:24', 3),
(1182, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:12:35', 3),
(1183, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '01:13:47', 3),
(1184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '01:16:08', 3),
(1185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '01:29:53', 3),
(1186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '01:30:28', 3),
(1187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '01:33:00', 3),
(1188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '01:33:10', 3),
(1189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-19', '11:16:45', 1),
(1190, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-10-19', '11:18:31', 1),
(1191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-19', '11:26:00', 1),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-19', '11:33:32', 1),
(1193, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-10-19', '11:33:36', 1),
(1194, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-10-19', '11:38:54', 1),
(1195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '11:36:43', 1),
(1196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '11:39:30', 1),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '11:42:45', 1),
(1198, 'DESATIVOU O LOGIN 8', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''8''', '2016-10-25', '11:42:51', 1),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '15:45:21', 1),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '19:02:11', 1),
(1201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '12:52:15', 1),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '12:53:57', 1),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '13:00:58', 1),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '13:01:14', 1),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '13:01:25', 1),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '13:02:25', 1),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:21:36', 1),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:22:05', 1),
(1209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:23:19', 1),
(1210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:23:51', 1),
(1211, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:24:28', 1),
(1212, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:25:02', 1),
(1213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:25:22', 1),
(1214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:25:41', 1),
(1215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:26:06', 1),
(1216, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:26:21', 1),
(1217, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-31', '14:26:42', 1),
(1218, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-01', '11:18:36', 1),
(1219, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '16:40:29', 1),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:14:20', 1),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:15:29', 1),
(1222, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:20:23', 1),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:25:32', 1),
(1224, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:28:14', 1),
(1225, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:32:11', 1),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:34:37', 1),
(1227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-03', '19:36:28', 1),
(1228, 'CADASTRO DO LOGIN ', '', '2016-11-04', '14:23:47', 1),
(1229, 'ALTERAÇÃO DO LOGIN 4', '', '2016-11-04', '14:25:38', 1),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-22', '10:58:03', 1),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '14:41:59', 4),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-09', '12:09:45', 1),
(1233, 'ALTERAÇÃO DO CLIENTE ', '', '2017-03-20', '13:01:11', 1),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2017-04-03', '14:23:44', 1),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2017-06-19', '08:44:44', 1),
(1236, 'ALTERAÇÃO DO CLIENTE ', '', '2017-06-19', '08:45:21', 1),
(1237, 'ALTERAÇÃO DO CLIENTE ', '', '2017-06-19', '08:47:57', 1),
(1238, 'ALTERAÇÃO DO CLIENTE ', '', '2017-06-19', '11:13:17', 1),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2017-06-20', '16:18:31', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_lojas`
--

CREATE TABLE IF NOT EXISTS `tb_lojas` (
  `idloja` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext,
  PRIMARY KEY (`idloja`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=4 ;

--
-- Fazendo dump de dados para tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_noticias`
--

CREATE TABLE IF NOT EXISTS `tb_noticias` (
  `idnoticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idnoticia`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=51 ;

--
-- Fazendo dump de dados para tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'UNIMED loca tendas da Reisforts', '<p>\r\n	Unimed Meia Maratona de Goi&acirc;nia, uma tradi&ccedil;&atilde;o no calend&aacute;rio esportivo do Estado, chega &agrave; 6&ordf; edi&ccedil;&atilde;o a plenos pulm&otilde;es. Uma prova &uacute;nica, para quem realmente gosta de superar limites. Muita sa&uacute;de, bem-estar, lazer e alegria para comemorar o anivers&aacute;rio de Goi&acirc;nia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O evento ocorreu no dia 19/10/2014 no estacionamento do Flamboyant e contou com algumas Tendas Piramidais e Chap&eacute;u de Bruxa da Reisforts.</p>', '1010201604193740883548..jpg', 'SIM', NULL, 'unimed-loca-tendas-da-reisforts', '', '', '', NULL),
(50, 'Animal Kids Farm Zoo', '<p>\r\n	Leve as crian&ccedil;as para conhecer o Animal Kids Farm Zoo e aproveite para apreciar as nossas tendas! O Evento que &eacute; um sucesso e j&aacute; ocorreu em demais shoppings da cidade agora est&aacute; com as tendas da Reisforts. Est&aacute; acontecendo de agora at&eacute; o dia 17 de agosto no estacionamento do Shopping Flamboyant.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&quot;Atualmente, a metr&oacute;pole se afasta cada vez mais de sua refer&ecirc;ncia rural, isto parece &oacute;bvio, mas n&atilde;o irrelevante: esta refer&ecirc;ncia e conhecimento s&atilde;o necess&aacute;rios principalmente para as novas gera&ccedil;&otilde;es. As crian&ccedil;as da cidade muitas vezes n&atilde;o conhecem &ldquo;ao vivo&rdquo; muitas esp&eacute;cies de animais, mesmo os dom&eacute;sticos: mini vacas, mini cavalos ou ainda animais ex&oacute;ticos como a lhama, b&uacute;falo, fur&atilde;o, chinchila e etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Isto nos motivou a criar a exposi&ccedil;&atilde;o tem&aacute;tica, Animal Kids Farm Zoo que traz animais vivos para entretenimento, seja para serem vistos e interagir com os mesmos. Mini-cavalos, mini-bovinos, mini-cabras, mini-coelhos, lhama, macacos, araras, tucanos, fur&atilde;o,chinchila, porquinho da &iacute;ndia, hamster, cavalos com sela (para foto), charrete (para passeio), carro de boi e muito mais!&quot;</p>', '1010201604215792909525..jpg', 'SIM', NULL, 'animal-kids-farm-zoo', '', '', '', NULL),
(48, 'Nova filial em Anápolis-GO', '<div>\r\n	A Reisforts expande cada vez mais para ficar mais pr&oacute;ximo do seu cliente, agora com um novo endere&ccedil;o em An&aacute;polis-GO. Desde agora os produtos j&aacute; est&atilde;o dispon&iacute;veis para essa regi&atilde;o sem frete, logo mais haver&aacute; um escrit&oacute;rio comercial atendendo no local atrav&eacute;s do endere&ccedil;o Rua Arlindo Costa, n. 126, Bairro Jundia&iacute;, An&aacute;polis - GO.</div>', '3009201605141181744172..jpg', 'SIM', NULL, 'nova-filial-em-anapolisgo', '', '', '', NULL),
(47, 'Novo Produto - Banheiro com Chuveiro!', '<p>\r\n	Agora contamos com o inovador banheiro para banho que vem com um chuveiro! &Eacute; uma &oacute;tima alternativa de baixo custo para locais tempor&aacute;rios, como: constru&ccedil;&atilde;o, acampamentos, festas e etc. &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	N&atilde;o necessita de caixa de &aacute;gua e &eacute; facilmente conectado a rede de &aacute;gua. O chuveiro el&eacute;trico &eacute; bivolt (110/220 volts). Temos os chuveiros dispon&iacute;veis nas cores: Azul, verde e laranja.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Somos a primeira empresa a fornecer banheiros port&aacute;teis com chuveiro para loca&ccedil;&atilde;o em Goi&acirc;nia e An&aacute;polis, ligue para n&oacute;s e fa&ccedil;a j&aacute; o seu or&ccedil;amento!</p>', '1010201604234923592093..jpg', 'SIM', NULL, 'novo-produto--banheiro-com-chuveiro', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_parceiros`
--

CREATE TABLE IF NOT EXISTS `tb_parceiros` (
  `idparceiro` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idparceiro`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=19 ;

--
-- Fazendo dump de dados para tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(3, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_portifolios` (
  `idportfolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idportfolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=8 ;

--
-- Fazendo dump de dados para tabela `tb_portifolios`
--

INSERT INTO `tb_portifolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qtd_visitas` int(20) NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=27 ;

--
-- Fazendo dump de dados para tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `qtd_visitas`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`) VALUES
(9, 'Tendas Estrelar', '1010201611405832250296..jpg', '<p>\r\n	Ideal para promo&ccedil;&otilde;es e eventos, para qualquer ocasi&atilde;o que necessite de um design mais sofisticado e leve. A tenda Estrelar &eacute; uma novidade no mercado e a ReisForts produz o melhor tipo dessa tenda no mercado. Fabricado em lona de tecido sint&eacute;tico resistente, anti mofo, n&atilde;o propagador de chamas e com aplica&ccedil;&atilde;o de laca anti UV, garantindo maior durabilidade do produto. Pode ser personalizado nas cores de silks e nas lonas brancas e transparentes (como a tenda cristal).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pode ser fabricana em Lona PVC de material extra dur&aacute;vel, aditivado contra raios UV e oxida&ccedil;&atilde;o, cont&eacute;m blackout (impedindo 40% do calor), n&atilde;o propagador de chamas, anti mofo, anti ressecamento, e imperme&aacute;vel. Ou por Lona PVC transl&uacute;cida de material extra-dur&aacute;vel, n&atilde;o propagador de chamas, aditivado contra oxida&ccedil;&atilde;o, anti mofo, anti ressecamento e imperme&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>USO RECOMENDADO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Promo&ccedil;&atilde;o de eventos na &aacute;rea de esportes, moda, design, arquitetura, arte e etc. Tudo que necessite de uma tenda com um design mais sofisticado.</p>', 75, 21, '', '', '', 'SIM', 0, 'tendas-estrelar', 0, '4545454545454545', NULL, NULL, NULL, NULL, '111222233333', 'SIM'),
(10, 'Tendas Estrelar Estendida', '1010201611441179449682..jpg', '<p>\r\n	A tenda Estrelar Estendida &eacute; um sucesso recente feito pela Reisforts. A tenda tem o modelo da Estrelar s&oacute; que estendida, alcan&ccedil;ando uma &aacute;rea maior de prote&ccedil;&atilde;o. A Estrelar Estendida &eacute; usada para eventos que necessitam de um modelo diferenciado de tenda, mas com a prote&ccedil;&atilde;o de uma tenda convencional.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lona feita em PVC Blackout (tratamento anti chamas, anti fungos e anti u.v) - &nbsp;O material utilizado &eacute; altamente resistente e feito com um plastificante especial que facilita a limpeza e dificulta o ac&uacute;mulo de sujeira, al&eacute;m de aditivos que garantem uma durabilidade &uacute;nica. Essa &eacute; a lona mais resistente do mercado. Ferragens galvanizadas fabricadas na chapa 16 de grande resist&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tamanho da tenda: 13x19 (247m&sup2;) - produzimos tamanhos diferenciados sob encomenda.</p>', 75, 2, '', '', '', 'SIM', 0, 'tendas-estrelar-estendida', 0, NULL, NULL, NULL, NULL, NULL, '0111111', 'SIM'),
(11, 'Tendas Camping', '1010201611481120968754..jpg', '<p>\r\n	&Eacute; a tenda ideal para acampar, oferecendo mais conforto e seguran&ccedil;a do que as barracas tradicionais. Feita de uma estrutura sanfonada que possuem montagem r&aacute;pida (at&eacute; 60s) o segredo est&aacute; nas lonas laterais e inferiores que postas formam uma barraca. Com portas feitas com duplo zipper (para abertura interna e externa), janelas e telas de prote&ccedil;&atilde;o contra insetos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Tenda Camping da Reisforts &eacute; feita com cobertura em lona PVC ou Nylon 600 (importado, emborrachado e imperme&aacute;vel), e as ferragens s&atilde;o fabricadas na chapa 18.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Produto dispon&iacute;vel nos tamanhos: &nbsp;2X2 (4m&sup2;) , 3X2 (6m&sup2;), 3X3 (9m&sup2;), 4,5X3 (13,5m&sup2;) e 6X3 (18m&sup2;). Cobertura em lona PVC ou Nylon 600 (importado, emborrachado e imperme&aacute;vel).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ferragens fabricadas na chapa 18 (grande resist&ecirc;ncia)</p>\r\n<p>\r\n	Ferragens galvanizadas (anti ferrugem)</p>\r\n<p>\r\n	Costura refor&ccedil;ada nos cantos e partes de maior atrito da lona com as ferragens</p>', 75, 3, '', '', '', 'SIM', 0, 'tendas-camping', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(12, 'Tendas Cobertura Mesclada', '1010201611518335745549..jpg', '<p>\r\n	Al&eacute;m das tendas personalizadas a Reisforts disponibiliza uma op&ccedil;&atilde;o de sanfonada diferenciada. A Tenda Sanfonada com Cobertura Mesclada. &Eacute; uma tenda Sanfonada de montagem r&aacute;pida (at&eacute; 60s) com a cobertura dividida em cores diferentes.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Tenda Cobertura Mesclada da Reisforts &eacute; feita com cobertura em lona Nylon 600 (importado, emborrachado e imperme&aacute;vel), e as ferragens s&atilde;o fabricadas na chapa 18.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tendas dispon&iacute;veis nos tamanhos: 4,5x3 (13,5m&sup2;)&nbsp;</p>\r\n<p>\r\n	Cobertura Nylon 600</p>\r\n<p>\r\n	Fabricados na chapa 18</p>', 75, 6, '', '', '', 'SIM', 0, 'tendas-cobertura-mesclada', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(13, 'Tendas Cristal', '1010201611566154274394..jpg', '<p>\r\n	A tenda Cristal pode ser fabricada com tr&ecirc;s estruturas: dentre elas Piramidal, Calhada e Tensionada, sendo feita uma cobertura em lona transl&uacute;cida. Pode ser desenvolvida sob-encomenda tamb&eacute;m em formatos especiais com medidas adaptadas ao ambiente a ser coberto, e tamb&eacute;m nos formatos octagonal, decagonal, entre outros.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lona PVC transl&uacute;cida de material extra-dur&aacute;vel, n&atilde;o propagador de chamas, aditivado contra oxida&ccedil;&atilde;o, anti mofo, anti ressecamento e imperme&aacute;vel. Jun&ccedil;&atilde;o por r&aacute;dio frequ&ecirc;ncia e refor&ccedil;os nos pontos de maior desgaste, garantindo maior durabilidade. Ferragens galvanizadas tendas &nbsp;piramidais fabricadas na chapa 14 e 16 (grande resist&ecirc;ncia). Incluso estacas e cordas para fixa&ccedil;&atilde;o.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tamanhos dispon&iacute;veis para venda: 3x3 (9m&sup2;), 4x4 (16m&sup2;), 5x5 (25m&sup2;), 6x6 (36m&sup2;), 8x8 (64m&sup2;), 10x10 (100m&sup2;) e 12x12 (144m&sup2;).</p>', 75, 1, '', '', '', 'SIM', 0, 'tendas-cristal', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(14, 'Tenda Personalizada / Silkada', '1010201611585709216640..jpg', '<p>\r\n	Dispon&iacute;vel em diversos tamanhos e cores, a Tenda Personalizada (Sanfonada) &eacute; uma excelente op&ccedil;&atilde;o para tornar o seu evento seguro e confort&aacute;vel. &nbsp;Todas as tendas sanfonadas permitem personaliza&ccedil;&atilde;o de logomarca, deixando sua tenda com uma apar&ecirc;ncia &uacute;nica, e assim seu cliente pode lhe identificar com maior facilidade. Ideal para clientes que precisam de exibir a sua marca em eventos r&aacute;pidos, como: estandes promocionais, merchandising, feiras, exposi&ccedil;&otilde;es e outros.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Tenda Personalizada (Sanfonada) da Reisforts &eacute; feita com cobertura em lona PVC ou Nylon 600 (importado, emborrachado e imperme&aacute;vel), e as ferragens s&atilde;o fabricadas na chapa 18.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cobertura em lona PVC ou Nylon 600</p>\r\n<p>\r\n	Dispon&iacute;vel nos tamanhos: 2x2 (4m&sup2;), 3x2 (6m&sup2;), 3X3 (9m&sup2;), 4,5x3 (13,5m&sup2;) e 6x3 (18m&sup2;)</p>\r\n<div>\r\n	&nbsp;</div>', 75, 11, '', '', '', 'SIM', 0, 'tenda-personalizada--silkada', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(15, 'Tenda Piramidal', '1110201612016813432284..jpg', '<p>\r\n	&Eacute; a tenda mais usada no Brasil, por ter uma montagem r&aacute;pida &eacute; ideal para todo tipo de evento, seja ele de pequeno ou grande porte. Pode ser fabricada em tamanhos diferenciados sob-encomenda. Cont&eacute;m galvaniza&ccedil;&atilde;o super resistente que impede o aparecimento de ferrugens. &Eacute; totalmente montada por encaixe e unida com parafusos e conex&otilde;es em a&ccedil;o inoxid&aacute;vel.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cobertura tenda piramidal em lona PVC TD1000 Blackout (tratamento anti chamas, anti fungos e anti u.v). Ferragens galvanizadas fabricadas na chapa 14 e 16 (grande resist&ecirc;ncia). Incluso estacas e cordas para fixa&ccedil;&atilde;o.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tamanhos dispon&iacute;veis para venda: 3x3 (9m&sup2;), 4x4 (16m&sup2;), 5x5 (25m&sup2;), 6x6 (36m&sup2;), 8x8 (64m&sup2;), 10x10 (100m&sup2;) e 12x12 (144m&sup2;). &nbsp;Al&eacute;m dos tamanhos padr&otilde;es acima citados, a tenda pode ser personalizada no tamanho de seu espa&ccedil;o.</p>\r\n<div>\r\n	&nbsp;</div>', 75, 12, '', '', '', 'SIM', 0, 'tenda-piramidal', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(16, 'Tendas Sanfonada', '1110201612057679729047..jpg', '<p>\r\n	&Eacute; uma tenda port&aacute;til, oferecendo sombra e conforto de imediato por sua montagem super r&aacute;pida. Sendo montada em apenas 60s. A Sanfonada recebe esse nome por se tratar de um modelo dobr&aacute;vel, lembrando uma sanfona. Ideal para eventos como: Workshop, ponto de venda, assessoria esportiva, feiras, divulga&ccedil;&otilde;es, lazer e etc. Todas as tendas sanfonadas permitem personaliza&ccedil;&atilde;o de logomarca, deixando sua tenda com uma apar&ecirc;ncia &uacute;nica.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Tenda Sanfonada da Reisforts &eacute; feita com cobertura em lona PVC ou Nylon 600 (importado, emborrachado e imperme&aacute;vel), e as ferragens s&atilde;o fabricadas na chapa 18.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tendas dispon&iacute;veis nos tamanhos: 2x2 (4m&sup2;), 3x2 (6m&sup2;), 3x3 (9m&sup2;), 4x4 (16m&sup2;), 4,5x3 (13,5m&sup2;) e 6x3 (18m&sup2;)</p>', 75, 13, '', '', '', 'SIM', 0, 'tendas-sanfonada', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(17, 'Tendas Sombrite', '0912201612096177511492..jpg', '<p>\r\n	Os Sombrites s&atilde;o telas termo pl&aacute;sticas que foram desenvolvidas para oferecer prote&ccedil;&atilde;o contra o calor e os raios UV solares. A tela sombreadora apresenta excelente versatilidade para se adequar aos diferentes projetos arquitet&ocirc;nicos sempre conferindo um design arrojado de grande beleza, sendo ideal para coberturas de estacionamentos..</p>', 75, 16, '', '', '', 'SIM', 0, 'tendas-sombrite', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(18, 'Tenda Tensionada Árabe', '1110201612296374607429..jpg', '<p>\r\n	A tenda Tensionada, &Aacute;rabe ou Chap&eacute;u de Bruxa trata-se de uma cobertura com um dos designes mais harmoniosos e atrativos do mercado. De instala&ccedil;&atilde;o r&aacute;pida, possui estrutura met&aacute;lica de travamento com parafusos e sistema de bitola de encaixe. O nome Tensionada vem do seu sistema de cabo de a&ccedil;o que tensionados d&atilde;o sustenta&ccedil;&atilde;o a cobertura que tensiona a tenda, fazendo com que ela tenha esse formato peculiar.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lona PVC calandrado de material extra dur&aacute;vel, aditivado contra raios ultra violeta (UV) e oxida&ccedil;&atilde;o, cont&eacute;m blackout (impedindo 40% do calor), n&atilde;o propagador de chamas, anti mofo, anti ressecamento, e imperme&aacute;vel. Jun&ccedil;&atilde;o com r&aacute;dio frequ&ecirc;ncia e refor&ccedil;os nos pontos de maior desgaste, garantindo maior durabilidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tamanhos dispon&iacute;veis para venda: 3x3 (9m&sup2;), 4x4 (16m&sup2;), 5x5 (25m&sup2;), 6x6 (36m&sup2;), 8x8 (64m&sup2;) e 10x10 (100m&sup2;)&nbsp;</p>', 75, 14, '', '', '', 'SIM', 0, 'tenda-tensionada-arabe', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(19, 'Banheiro para Banho', '1110201612359885325907..jpg', '<p>\r\n	Chega em Goi&acirc;nia e An&aacute;polis uma novidade &uacute;nica apenas comercializada pela Reisforts: O banheiro port&aacute;til com chuveiro. O banheiro &eacute; composto por luz, teto transl&uacute;cido, piso antiderrapante e conta tamb&eacute;m com apoio de objetos. Esse banheiro para banho &eacute; uma alternativa de baixo custo que oferece uma maior comodidade para constru&ccedil;&otilde;es civis, eventos e outros. &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Agora dispon&iacute;vel tamb&eacute;m para Venda!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Banheiro para Banho possu&iacute;:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Identifica&ccedil;&atilde;o de masculino e feminino</p>\r\n<p>\r\n	Piso antiderrapante</p>\r\n<p>\r\n	Ponto de ventila&ccedil;&atilde;o interna</p>\r\n<p>\r\n	Trava Interna</p>\r\n<p>\r\n	Facilmente conectado a rede de &aacute;gua</p>\r\n<p>\r\n	N&atilde;o necessita de caixa de &aacute;gua</p>\r\n<p>\r\n	Para uso tempor&aacute;rio ou permanente</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>USO RECOMENDADO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esse banheiro para banho &eacute; uma alternativa de baixo custo que oferece uma maior comodidade para constru&ccedil;&otilde;es civis, eventos e outros.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Altura: 2,30m</p>\r\n<p>\r\n	Largura: 1,10m</p>\r\n<p>\r\n	Comprimento: 1,20m</p>\r\n<p>\r\n	Peso: 75kg</p>\r\n<p>\r\n	Cap. do reservat&oacute;rio: 227L</p>\r\n<p>\r\n	Chuveiro El&eacute;trico 110/220 volts</p>\r\n<p>\r\n	Dispon&iacute;vel nas cores: Azul, Verde e Laranja</p>', 89, 15, '', '', '', 'SIM', 0, 'banheiro-para-banho', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(20, 'Banheiro Standard', '1110201612388756616359..jpg', '<p>\r\n	Com a facilidade de se adaptar a qualquer ambiente, o banheiro qu&iacute;mico REISFORTS &eacute; de f&aacute;cil utiliza&ccedil;&atilde;o para o usu&aacute;rio e f&aacute;cil locomo&ccedil;&atilde;o, n&atilde;o necessita de qualquer tipo de instala&ccedil;&atilde;o hidro-sanit&aacute;ria, al&eacute;m de proteger e contribuir com a preserva&ccedil;&atilde;o do meio-ambiente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Agora dispon&iacute;vel tamb&eacute;m para Venda!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Banheiro Standard possu&iacute;:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Identifica&ccedil;&atilde;o de masculino e feminino</p>\r\n<p>\r\n	Piso antiderrapante</p>\r\n<p>\r\n	Ponto de ventila&ccedil;&atilde;o interna</p>\r\n<p>\r\n	Reservat&oacute;rio interno de dejetos</p>\r\n<p>\r\n	Mict&oacute;rio</p>\r\n<p>\r\n	Suporte para papel higi&ecirc;nico</p>\r\n<p>\r\n	Trava Interna</p>\r\n<p>\r\n	Alguns modelos cont&eacute;m espelho interno (Banheiros Laranjas)</p>\r\n<p>\r\n	Dispon&iacute;vel nas cores Rosa, Azul, Verde e Laranja</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Dimens&otilde;es:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Altura: 2,30m</p>\r\n<p>\r\n	Largura: 1,10m</p>\r\n<p>\r\n	Comprimento: 1,20m</p>\r\n<p>\r\n	Peso: 75kg</p>\r\n<p>\r\n	Cap. do reservat&oacute;rio: 227L</p>\r\n<div>\r\n	&nbsp;</div>', 89, 17, '', '', '', 'SIM', 0, 'banheiro-standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(21, 'Banheiro VIP', '1110201612414125184015..jpg', '<p>\r\n	O banheiro qu&iacute;mico VIP REISFORTS &eacute; a op&ccedil;&atilde;o perfeita para camarim, ou evento mais sofisticado. N&atilde;o necessita de qualquer tipo de instala&ccedil;&atilde;o hidro-sanit&aacute;ria, possui &nbsp;pia com &aacute;gua, dispensers para alcool em gel ou sabonete liquido e sistema de descarga. &Eacute; de f&aacute;cil utiliza&ccedil;&atilde;o para o usu&aacute;rio e f&aacute;cil locomo&ccedil;&atilde;o, al&eacute;m de proteger e contribuir com a preserva&ccedil;&atilde;o do meio-ambiente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Agora dispon&iacute;vel tamb&eacute;m para Venda!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Banheiro VIP possu&iacute;:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Identifica&ccedil;&atilde;o de masculino e feminino</p>\r\n<p>\r\n	Piso antiderrapante</p>\r\n<p>\r\n	Porta Sabonete</p>\r\n<p>\r\n	Dispensers</p>\r\n<p>\r\n	Pia com reservat&oacute;rio de &aacute;gua</p>\r\n<p>\r\n	Porta Objetos</p>\r\n<p>\r\n	Suporte para papel higi&ecirc;nico</p>\r\n<p>\r\n	Descarga</p>\r\n<p>\r\n	Ponto de ventila&ccedil;&atilde;o interna</p>\r\n<p>\r\n	Reservat&oacute;rio interno de dejetos</p>\r\n<p>\r\n	Mict&oacute;rio</p>\r\n<p>\r\n	Trava Interna</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Dimens&otilde;es:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Altura: 2,30m</p>\r\n<p>\r\n	Largura: 1,10m</p>\r\n<p>\r\n	Comprimento: 1,20m</p>\r\n<p>\r\n	Peso: 75kg</p>\r\n<p>\r\n	Cap. do reservat&oacute;rio: 227L</p>', 89, 18, '', '', '', 'SIM', 0, 'banheiro-vip', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'NAO'),
(22, 'Banheiro PNE', '1110201612438149461874..jpg', '<p>\r\n	Os banheiros qu&iacute;micos PNE (Portadores de Necessidades Especiais) s&atilde;o dotados de um tamanho diferenciado que proporciona o conforto para o usu&aacute;rio que pode manobrar a cadeira de rodas dentro do mesmo. Ademais, possuem barras de apoio obedecendo as mais r&iacute;gidas regras de acessibilidade. Pensando na comodidade de nossos clientes a Reisforts trouxe esta novidade para Goi&acirc;nia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Agora dispon&iacute;vel tamb&eacute;m para Venda!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Banheiro PNE possu&iacute;:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Corrim&atilde;o</p>\r\n<p>\r\n	Piso antiderrapante</p>\r\n<p>\r\n	Porta objetos</p>\r\n<p>\r\n	Ponto de ventila&ccedil;&atilde;o interna</p>\r\n<p>\r\n	Reservat&oacute;rio interno de dejetos</p>\r\n<p>\r\n	Suporte para papel higi&ecirc;nico</p>\r\n<p>\r\n	Trava Interna</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>USO RECOMENDADO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Eventos em &aacute;reas abertas que n&atilde;o h&aacute; disponibilidade de banheiros ou que necessitem de banheiros extras para portadores de necessidades especiais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cont&eacute;m um corrim&atilde;o pr&oacute;prio para portadores de necessidades especiais que necessitem do uso de cadeiras de rodas e etc.Cont&eacute;m um corrim&atilde;o pr&oacute;prio para portadores de necessidades especiais que necessitem do uso de cadeiras de rodas e etc.</p>', 89, 19, '', '', '', 'SIM', 0, 'banheiro-pne', 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(23, 'Balcão Sanfonada', '1110201612529820599518..jpg', '<p>\r\n	Balc&atilde;o Sanfonada &eacute; um balc&atilde;o usado no interior da tenda sanfonada para v&aacute;rias utilidades. O Balc&atilde;o da Reisforts &eacute; feito de ferro &ldquo;Metalon&rdquo; em a&ccedil;o carbono com tratamento &agrave; zinco (galvaniza&ccedil;&atilde;o) anti ferrugem de alta resist&ecirc;ncia. N&atilde;o possu&iacute; pe&ccedil;as m&oacute;veis ou soltas, porca auto tratantes.Tendo como vantagens a portabilidade, praticidade de montagem, beleza e resist&ecirc;ncia, o balc&atilde;o sanfonado tornou-se um sucesso de vendas e uma ferramenta indispens&aacute;vel de trabalho para v&aacute;rios clientes em todo o pa&iacute;s.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>USO RECOMENDADO</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Usa-se para assentar objetos no interior da tenda de uma forma pr&aacute;tica, para evitar carregar mesas junto com a tenda, o balc&atilde;o &eacute; pr&aacute;tico e montado facilmente. Muito recomendado para quem necessita de expor um produto ou servi&ccedil;o como: Feiras, Estandes Promocionais, Merchandising e etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&Eacute; vendido nas medidas de:</p>\r\n<p>\r\n	2x0,80x1,00 (C,L,H) metros</p>\r\n<p>\r\n	3x0,80x1,00 (C,L,H) metros</p>', 90, 20, '', '', '', 'SIM', 0, 'balcao-sanfonada', 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(24, 'Containers', '1110201612554410208956..jpg', '<p>\r\n	A Container e Cia oferece uma inova&ccedil;&atilde;o em container tipo Box. Estrutura toda desmont&aacute;vel que facilita seu transporte. Pain&eacute;is met&aacute;licos que podem ser substitu&iacute;dos por outros materiais ampliando assim a sua utilidade. &nbsp;Produzidos com dimens&otilde;es espec&iacute;ficas para o seu uso, podem conter de &nbsp;4 a 30 m&sup3;. &Uacute;teis para constru&ccedil;&atilde;o civil, armazenamento de produtos, transporte de carga via rodovia e mar&iacute;tima. Pode ser utilizado at&eacute; mesmo para criar pequenos espa&ccedil;os para pontos de vendas, escrit&oacute;rios, banheiros, dep&oacute;sitos e etc.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Pode conter de 4 a 30 m&sup3;, pain&eacute;is met&aacute;licos podem ser substitu&iacute;dos.&nbsp;</p>\r\n<p>\r\n	Peso aproximado de 400kg</p>', 91, 22, '', '', '', 'SIM', 0, 'containers', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM'),
(25, 'Piso Elevado', '1110201601061445637411..jpg', '<p>\r\n	O Piso Elevado &eacute; fabricado utilizando laminado naval espec&iacute;fico para suportar grandes volumes e pesos, al&eacute;m de poder ser molhado sem danificar a estrutura, permitindo sua montagem ao ar livre. Medida padr&atilde;o do Piso Elevado &eacute; 2,20x1,60m. Montado de acordo com o layout do cliente e alturas variadas. As alturas variam de 15cm do solo at&eacute; grandes camarotes que chegam a 2m do solo. &Eacute; regul&aacute;vel de 5 em 5cm proporcionando assim um total nivelamento do solo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>CARACTER&Iacute;STICAS</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Suporta grandes volumes e pesos</p>\r\n<p>\r\n	&Aacute;gua n&atilde;o danifica a estrutura</p>\r\n<p>\r\n	Alturas variadas</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tamanho: 2,20 x 1,60m</p>\r\n<p>\r\n	Altura: varia de 15cm at&eacute; 2m&nbsp;</p>\r\n<p>\r\n	Regul&aacute;vel de 5 em 5cm</p>', 92, 23, '', '', '', 'SIM', 0, 'piso-elevado', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'NAO'),
(26, 'Gradil', '1110201601163202265712..jpg', '<p>\r\n	As Barricadas/Gradil s&atilde;o extremamente &uacute;teis para limitar espa&ccedil;os e para fazer a conten&ccedil;&atilde;o de espectadores em shows, est&aacute;dios de futebol, feiras e eventos p&uacute;blicos. As Barricadas possuem grande resist&ecirc;ncia, podendo ser fixadas individualmente ao solo ou soldadas a estruturas de pisos elevados. Duas medida: &nbsp;1,20 x 2m ou 1,20 x 3m de comprimento. S&atilde;o fabricadas com cantos arredondados minorando, assim, os riscos de eventuais ferimentos aos usu&aacute;rios e montadores. Possuem sistemas de travas de encaixe que permitem &nbsp;montagem e desmontagem r&aacute;pida e segura.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>ESPECIFICA&Ccedil;&Otilde;ES</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Duas medidas: 1,20 x 2m ou 1,20 x 3m de comprimento</p>\r\n<p>\r\n	Bordas arredondadas</p>\r\n<p>\r\n	Sistemas de travas de encaixe</p>\r\n<p>\r\n	Dispon&iacute;vel apenas para Venda</p>', 93, 24, '', '', '', 'SIM', 0, 'gradil', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos_aplicacoes`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_aplicacoes` (
  `idprodutoaplicacao` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  PRIMARY KEY (`idprodutoaplicacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=16 ;

--
-- Fazendo dump de dados para tabela `tb_produtos_aplicacoes`
--

INSERT INTO `tb_produtos_aplicacoes` (`idprodutoaplicacao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `titulo`) VALUES
(11, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 1'),
(12, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 2'),
(13, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 3'),
(14, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 4'),
(15, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 5');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  PRIMARY KEY (`idprodutosmodeloaceito`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Fazendo dump de dados para tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(3, 16, 1, 6, 4, 1987, 2013, 'SIM', 0, ''),
(4, 16, 1, 7, 5, 1987, 2013, 'SIM', 0, ''),
(5, 16, 1, 8, 6, 1987, 2013, 'SIM', 0, ''),
(6, 16, 1, 9, 7, 1987, 2013, 'SIM', 0, ''),
(7, 15, 1, 7, 5, 2000, 2016, 'SIM', 0, ''),
(8, 15, 1, 8, 6, 2000, 2016, 'SIM', 0, ''),
(10, 2, 1, 6, 3, 1990, 2016, 'SIM', 0, ''),
(11, 4, 3, 5, 2, 1994, 2004, 'SIM', 0, ''),
(12, 3, 1, 7, 5, 1980, 2016, 'SIM', 0, ''),
(13, 17, 8, 7, 5, 2009, 2013, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_seo`
--

CREATE TABLE IF NOT EXISTS `tb_seo` (
  `idseo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idseo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=11 ;

--
-- Fazendo dump de dados para tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Reisforts - Venda e Aluguel de Tendas Goiânia e Banheiros Químicos Goiânia - Goiás.', 'A Reisforts Fabricação, Venda e Aluguel de Tendas e Banheiros Químicos Goiânia, é uma empresa voltada para a fabricação e vendas de tendas e banheiros químicos, que atende as regiões de Brasília - DF, cidade do interior de Goiás, Anápolis, Rio Verde, Caldas Novas, Catalão, Jataí, cidades do Tocantis, Palmas, Araguaína, o estado do Rio de Janeiro e São Paulo, Mato Grosso, Mato Grosso do Sul, Pará, Acre, Rondônia entre outras regiões. Que prima por uma atuação inovadora e ética, cuja qualidade é atestada por mais de 10 anos de mercado e uma clientela de renome como a Prefeitura de Goiânia, Prefeitura de Palmeiras, Ilha Pura, Serra Grande, ADS Construtora (Mato Grosso), Tribunal da Justiça de Goiás, Supermercado Bretas, entre outros.', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'SIM', NULL, 'index'),
(2, 'Empresa', 'Reisforts Goiânia - Venda e Aluguel de Tendas e Banheiros Químicos Goiânia - Goiás.', 'A Reisforts Fabricação, Venda e Aluguel de Tendas e Banheiros Químicos Goiânia, é uma empresa voltada para a fabricação e vendas de tendas e banheiros químicos, que atende as regiões de Brasília - DF, cidade do interior de Goiás, Anápolis, Rio Verde, Caldas Novas, Catalão, Jataí, cidades do Tocantis, Palmas, Araguaína, o estado do Rio de Janeiro e São Paulo, Mato Grosso, Mato Grosso do Sul, Pará, Acre, Rondônia entre outras regiões. Que prima por uma atuação inovadora e ética, cuja qualidade é atestada por mais de 10 anos de mercado e uma clientela de renome como a Prefeitura de Goiânia, Prefeitura de Palmeiras, Ilha Pura, Serra Grande, ADS Construtora (Mato Grosso), Tribunal da Justiça de Goiás, Supermercado Bretas, entre outros.', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'SIM', NULL, 'empresa'),
(3, 'Dicas', 'Reisforts Goiânia - Venda e Aluguel de Tendas e Banheiros Químicos Goiânia - Goiás.', 'A Reisforts Fabricação, Venda e Aluguel de Tendas e Banheiros Químicos Goiânia, é uma empresa voltada para a fabricação e vendas de tendas e banheiros químicos, que atende as regiões de Brasília - DF, cidade do interior de Goiás, Anápolis, Rio Verde, Caldas Novas, Catalão, Jataí, cidades do Tocantis, Palmas, Araguaína, o estado do Rio de Janeiro e São Paulo, Mato Grosso, Mato Grosso do Sul, Pará, Acre, Rondônia entre outras regiões. Que prima por uma atuação inovadora e ética, cuja qualidade é atestada por mais de 10 anos de mercado e uma clientela de renome como a Prefeitura de Goiânia, Prefeitura de Palmeiras, Ilha Pura, Serra Grande, ADS Construtora (Mato Grosso), Tribunal da Justiça de Goiás, Supermercado Bretas, entre outros.', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'SIM', NULL, 'dicas'),
(5, 'Orçamentos', 'Reisforts Goiânia Venda e Aluguel de Tendas e Banheiros Químicos - Orçamentos', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'SIM', NULL, 'orcamentos'),
(6, 'Notícias', 'Notícias', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Produtos', 'Reisforts Goiânia - Venda e Aluguel de Tendas e Banheiros Químicos - Produtos', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'SIM', NULL, 'produtos'),
(8, 'Fale Conosco', 'Fale Conosco', NULL, NULL, 'SIM', NULL, NULL),
(9, 'Trabalhe Conosco', 'Trabalhe Conosco', NULL, NULL, 'SIM', NULL, NULL),
(10, 'Serviços', 'Serviços', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'Tendas, Venda de Tendas, Aluguel de Tendas, Fabrica de Tendas, Fabricação de Tendas, Tendas Atacado, Distribuidor de Tendas, Tendas para Eventos, Aluguel de Tendas para eventos, Tendas para Festas, Aluguel de Tendas para Festas, Banheiro Químico, Venda de Banheiros Químicos, Aluguel de Banheiros Químicos, Fabrica de Banheiros Químicos, Fabricação de Banheiros Químicos, Banheiros Químicos Atacado, Distribuidor de Banheiros Químicos, Banheiros Químicos para Eventos, Aluguel de Banheiros Químicos para eventos, Banheiros Químicos para Festas, Aluguel de Banheiros Químicos para Festas, Tendas Goiânia, Venda de Tendas Goiânia, Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia, Venda de Tendas Goiânia , Venda de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Tendas Goiânia , Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas Goiânia, Tendas Atacado Goiânia, Distribuidor de Tendas Goiânia, Aluguel de Tendas Goiânia, Fabrica de Tendas Goiânia, Fabricação de Tendas, Tendas Goiânia Atacado, Goiânia Distribuidor de Tendas, Goiânia Tendas para Eventos, Aluguel de Tendas para eventos Goiânia, Tendas para Festas Goiânia, Aluguel de Tendas para Festas Goiânia, Banheiro Químico Goiânia, Venda de Banheiros Químicos Goiânia, Aluguel de Banheiros Químicos Goiânia, Fabrica de Banheiros Químicos Goiânia, Fabricação de Banheiros Químicos Goiânia, Banheiros Químicos Atacado Goiânia, Distribuidor de Banheiros Químicos Goiânia, Banheiros Químicos para Eventos Goiânia, Aluguel de Banheiros Químicos para eventos Goiânia, Banheiros Químicos para Festas Goiânia, Aluguel de Banheiros Químicos para Festas Goiânia.', 'SIM', NULL, 'servicos');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idservico`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=57 ;

--
-- Fazendo dump de dados para tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`, `imagem_principal`) VALUES
(5, 'Prensagem em mangueiras', '<p>\r\n	Prensagem em mangueiras e mangotes: Realizamos a prensagem das mangueiras hidr&aacute;ulicas atrav&eacute;s de maquinas que exercem press&atilde;o e inserem os terminais nas mangueiras, sendo calculada a press&atilde;o utilizada na coloca&ccedil;&atilde;o dos terminais hidr&aacute;ulicos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rainha da Borracha Goi&acirc;nia atende setores industriais, agr&iacute;cola, sucroalcooleiro e automotivo, fornecendo mangueiras montadas, conforme a necessidade de cada cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Estamos prontos para atend&ecirc;-lo!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'prensagem-em-mangueiras', '', '', '', 0, '', '', '0206201611176686009934.png', '2508201602594049189843.jpg'),
(6, 'Vulcanização em correias', '<p>\r\n	Vulcaniza&ccedil;&atilde;o em correias de borracha, PVC e PU.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rainha da Borracha atua no ramo de vulcaniza&ccedil;&atilde;o em correia transportadora, emenda a frio e emenda a quente, e possui a melhor equipe de profissionais do mercado com experi&ecirc;ncia e profissionalismo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Estamos prontos para atend&ecirc;-lo!</p>', '2707201609291160997649..jpg', 'SIM', NULL, 'vulcanização-em-correias', '', '', '', 0, '', '', '0206201605327691842909.png', '2508201603107466292261.jpg'),
(7, 'Emborrachamento de rolos', '<p>\r\n	Emborrachamento de rolos e roletes a frio e a quente para a aplica&ccedil;&atilde;o nas ind&uacute;strias de embalagens, couro, gr&aacute;fica, litogr&aacute;fica, metalgr&aacute;fica, siderurgica, pl&aacute;stico, t&ecirc;xtil, vidro, m&oacute;veis, papel e celulose e outros.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Todas as caracter&iacute;sticas t&eacute;cnicas das borrachas utilizadas, seguem rigorosamente as especifica&ccedil;&otilde;es de normas ASTM D-2000, ABNT EB 362 e SAE J 200 ou requisitos do cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Estamos prontos para atend&ecirc;-lo!</p>', '2707201609321298879006..jpg', 'SIM', NULL, 'emborrachamento-de-rolos', '', '', '', 0, '', '', '0206201611176686009934.png', '2508201603125891686056.jpg');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_subcategorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idsubcategoriaproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=25 ;

--
-- Fazendo dump de dados para tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Tenda Cristal', 75, 'SIM', NULL, 'tenda-cristal'),
(2, 'Tenda Estrelar Estendida', 75, 'SIM', NULL, 'tenda-estrelar-estendida'),
(3, 'Tenda Camping', 75, 'SIM', NULL, 'tenda-camping'),
(6, 'Tenda Cobertura Mesclada', 75, 'SIM', NULL, 'tenda-cobertura-mesclada'),
(11, 'Tenda Personalizada / Silkada', 75, 'SIM', NULL, 'tenda-personalizada--silkada'),
(12, 'Tenda Piramidal', 75, 'SIM', NULL, 'tenda-piramidal'),
(13, 'Tenda Sanfonada', 75, 'SIM', NULL, 'tenda-sanfonada'),
(14, 'Tenda Tensionada Árabe', 75, 'SIM', NULL, 'tenda-tensionada-arabe'),
(15, 'Banheiro para Banho', 89, 'SIM', NULL, 'banheiro-para-banho'),
(16, 'Tenda Sombrite', 75, 'SIM', NULL, 'tenda-sombrite'),
(17, 'Banheiro Standard', 89, 'SIM', NULL, 'banheiro-standard'),
(18, 'Banheiro VIP', 89, 'SIM', NULL, 'banheiro-vip'),
(19, 'Banheiro PNE', 89, 'SIM', NULL, 'banheiro-pne'),
(20, 'Balcão Sanfonada', 90, 'SIM', NULL, 'balcao-sanfonada'),
(21, 'Tenda Estrelar', 75, 'SIM', NULL, 'tenda-estrelar'),
(22, 'Containers', 91, 'SIM', NULL, 'containers'),
(23, 'Pisos', 92, 'SIM', NULL, 'pisos'),
(24, 'Gradil', 93, 'SIM', NULL, 'gradil');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_tipos_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  PRIMARY KEY (`idtipoveiculo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=11 ;

--
-- Fazendo dump de dados para tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `imagem`) VALUES
(1, 'Carros', 0, 'SIM', 'carros', '2006201608061191940846.png'),
(2, 'Motos', 0, 'SIM', 'motos', '2006201608071116286885.png'),
(3, 'Caminhões', 0, 'SIM', 'caminhoes', '2006201608071262816952.png'),
(8, 'Naúticos', 0, 'SIM', 'nauticos', '2006201608081392104809.png'),
(9, 'Nobreaks', 0, 'SIM', 'nobreaks', '2006201608091390486381.png'),
(10, 'Lâmpadas', 0, 'SIM', 'lampadas', '2006201608101362518866.png');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_unidades`
--

CREATE TABLE IF NOT EXISTS `tb_unidades` (
  `idunidade` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `src_place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  PRIMARY KEY (`idunidade`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=45 ;

--
-- Fazendo dump de dados para tabela `tb_unidades`
--

INSERT INTO `tb_unidades` (`idunidade`, `titulo`, `src_place`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'goiania', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d244583.7565257507!2d-49.444355989195444!3d-16.695828772934586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef6bd58d80867%3A0xef692bad20d2678e!2zR29pw6JuaWEsIEdP!5e0!3m2!1spt-BR!2sbr!4v1474129', 'SIM', NULL, 'goiania', NULL, NULL, NULL),
(2, 'Brasilia', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d244583.7565257507!2d-49.444355989195444!3d-16.695828772934586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef6bd58d80867%3A0xef692bad20d2678e!2zR29pw6JuaWEsIEdP!5e0!3m2!1spt-BR!2sbr!4v1474129', 'SIM', NULL, 'brasilia', NULL, NULL, NULL),
(3, 'são paulo', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3822.0458683248467!2d-49.28476368513354!3d-16.67458798850948!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5e02474a8cbf0ef0!2sRainha+da+Borracha!5e0!3m2!1spt-BR!2sus!4v1472135158080', 'SIM', NULL, 'são-paulo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_uniformes`
--

CREATE TABLE IF NOT EXISTS `tb_uniformes` (
  `iduniforme` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iduniforme`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=10 ;

--
-- Fazendo dump de dados para tabela `tb_uniformes`
--

INSERT INTO `tb_uniformes` (`iduniforme`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Uniformes de Saúdes', '2409201605381218447454.png', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'uniformes-de-saudes', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(8, 'Uniformes de Limpeza geral', '2409201605411332434728.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-de-limpeza-geral', NULL),
(9, 'Uniformes Cozinheiros', '2409201605401358107866.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-cozinheiros', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
