<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 limit 5");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                <div class="carousel-caption text-center">
                  <?php /*<h1><?php Util::imprime($imagem[titulo]); ?></h1>
                  <p><?php Util::imprime($imagem[legenda]); ?></p>*/ ?>
                  <?php if (!empty($imagem[url])) { ?>
                    <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                    <?php } ?>
                  </div>
                </div>

                <?php
                $i++;
              }
            }
            ?>


          </div>



          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <!-- <span class="fa fa-angle-left" aria-hidden="true"></span> -->
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <!-- <span class="fa fa-angle-right" aria-hidden="true"></span> -->
            <span class="sr-only">Next</span>
          </a>



        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- slider	-->
    <!-- ======================================================================= -->



    <div class="container">
      <div class="row tendas relativo">
        <!-- ======================================================================= -->
        <!-- NOSSAS TENDAS    -->
        <!-- ======================================================================= -->

        <div class="col-xs-12 lato-black top25">
          <h3>NOSSAS TENDAS</h3>
          <div class="barra_home top5"></div>
        </div>
        <!-- ======================================================================= -->
        <!-- NOSSAS TENDAS    -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- produtos somente com categoria tendas    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/produtos_tendas.php') ?>
        <!-- ======================================================================= -->
        <!-- produtos somente com categoria tendas    -->
        <!-- ======================================================================= -->
      </div>
    </div>


    <div class="container">
      <div class="row tendas relativo">
        <!-- ======================================================================= -->
        <!-- NOSSOS BANHEIROS    -->
        <!-- ======================================================================= -->

        <div class="col-xs-12 lato-black top25">
          <h3>NOSSOS BANHEIROS QUÍMICOS</h3>
          <div class="barra_home"></div>
        </div>

        <!-- ======================================================================= -->
        <!-- NOSSOS BANHEIROS    -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- produtos categoria banheiros    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/produtos_banheiros.php') ?>
        <!-- ======================================================================= -->
        <!-- produtos categoria banheiros    -->
        <!-- ======================================================================= -->

      </div>
    </div>



        <div class="container bg_empresa_home">
          <div class="row relativo">
            <div class="saiba_mais">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba_mais_home.png" alt="" />
            </div>
            <!-- ======================================================================= -->
            <!-- EMPRESA HOME    -->
            <!-- ======================================================================= -->
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

            <div class="col-xs-12">
              <div class="conheca_home top40">
                <h1>CONHEÇA MAIS A <span><?php Util::imprime($row[titulo]); ?></span></h1>
              </div>
              <div class="conheca_mais_home top20">
                <p><?php Util::imprime($row[descricao],1000); ?></p>
              </div>

              <div class="col-xs-4 padding0 top15">
                <a class="btn btn_conheca_mais col-xs-12" href="<?php echo Util::caminho_projeto() ?>/empresa"  title="SAIBA MAIS">SAIBA MAIS</a>
              </div>

            </div>
            <div class="clearfix"></div>
            <!-- ======================================================================= -->
            <!-- EMPRESA HOME    -->
            <!-- ======================================================================= -->


            <!--  ==============================================================  -->
            <!-- MAPA -->
            <!--  ==============================================================  -->
            <div class="col-xs-12 mapa_home padding0">
              <?php if (!empty($config[src_place])): ?>

                  <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="396" frameborder="0" style="border:0" allowfullscreen></iframe>

              <?php endif; ?>
            </div>
            <!--  ==============================================================  -->
            <!-- MAPA -->
            <!--  ==============================================================  -->


          </div>
        </div>



    <div class="container">
      <div class="row">
        <!-- ======================================================================= -->
        <!--NOSSAS NOTICIAS    -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top30">
          <h6>NOSSAS <span>NOTÍCIAS</span></h6>
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_personalizavel.png" alt="" class="input100"/>
        </div>

        <?php


        $result = $obj_site->select("tb_noticias","order by rand() limit 2");
        if(mysql_num_rows($result) == 0){
          echo "<h2 class='bg-info top25 ' style='padding: 20px; color:#000;'>Nenhum Notícia(s) encontrado.</h2>";
        }else{
          while ($row = mysql_fetch_array($result)) {
            ?>

            <div class="col-xs-6 noticias_geral">

              <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",534, 288, array("class"=>"input100 img-thumbnail", "alt"=>"$row[titulo]")) ?>

              <div class="noticias_desc top15">
                <p>
                  <?php Util::imprime($row[descricao],1000); ?>
                </p>
              </div>
            </a>


            </div>
            <?php
            if ($i == 4) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
          }
        }
        ?>


        <!-- ======================================================================= -->
        <!--NOSSAS NOTICIAS    -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!--pagamentos home    -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top40">
          <img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/forma_pagamentos_mobile.png" alt="" class="input100"/>
        </div>
        <!-- ======================================================================= -->
        <!--pagamentos home    -->
        <!-- ======================================================================= -->



      </div>
    </div>




    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->

  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>
