<div class="container">
  <div class="row top5 ">


    <!-- contatos topo  -->
    <div class="media col-xs-6 text-right h4-topo">
      <h6>VENDAS</h6>
      <div class="media-left lato-black">
        <h4 class="media-heading top5">	<i class="fa fa-whatsapp" aria-hidden="true"></i>
          <span><?php Util::imprime($config[ddd3]); ?></span><?php Util::imprime($config[telefone3]); ?></h4>
      </div>
      <div class="media-body">
        <a href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
          <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.jpg" alt="">
        </a>
      </div>
    </div>
    <!-- contatos topo  -->


    <?php if(!empty($config[telefone2])): ?>

      <!-- contatos topo  -->
      <div class="media col-xs-6 text-right h4-topo">
        <h6>LOCAÇÃO</h6>
        <div class="media-left lato-black">
          <h4 class="media-heading top5">
            <i class="fa fa-whatsapp" aria-hidden="true"></i>
            <span><?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?>
          </h4>
        </div>
        <div class="media-body">
          <a href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.jpg" alt="">
          </a>
        </div>
      </div>
      <!-- contatos topo  -->
    <?php endif; ?>


    <div class="col-xs-12 top10 padding0 fundo_azul">

      <!--  ==============================================================  -->
      <!-- logo -->
      <!--  ==============================================================  -->
      <div class="col-xs-8 top20 ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
        </a>
      </div>
      <!--  ==============================================================  -->
      <!-- logo -->
      <!--  ==============================================================  -->


      <div class="col-xs-4 padding0">
        <!-- ======================================================================= -->
        <!-- botao carrinho de compra -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 dropdown top20">
          <a class="btn btn_carrinho" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/carrinho_topo.png" alt="">
          </a>

          <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

            <?php
            if(count($_SESSION[solicitacoes_produtos]) > 0)
            {
              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
                <div class="lista-itens-carrinho">
                  <div class="col-xs-2">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                  </div>
                  <div class="col-xs-8 top10">
                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                  </div>
                  <div class="col-xs-1">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                  </div>
                  <div class="col-xs-12"><div class="borda_carrinho top5 bottom5">  </div> </div>
                </div>
                <?php
              }
            }





            if(count($_SESSION[solicitacoes_servicos]) > 0)
            {


              for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
              {
                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                ?>
                <div class="lista-itens-carrinho">
                  <div class="col-xs-2">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                  </div>
                  <div class="col-xs-8 top10">
                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                  </div>
                  <div class="col-xs-1">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                  </div>
                  <div class="col-xs-12"><div class="borda_carrinho top5 bottom5">  </div> </div>
                </div>
                <?php
              }
            }

            ?>

            <div class="col-xs-12 text-right top10 bottom20">
              <a class="btn btn_azul" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="ENVIAR ORÇAMENTO" >
                ENVIAR ORÇAMENTO
              </a>
            </div>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- botao carrinho de compra -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 top10 bottom15  text-right">
          <select name="menu" id="menu-site" class="select-menu">
            <option value=""></option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa">A EMPRESA</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/noticias">NOTÍCIAS</option>

            <option value="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</option>
          </select>
        </div>
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
      </div>
    </div>

  </div>
</div>
