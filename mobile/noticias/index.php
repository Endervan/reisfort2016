
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once("../includes/head.php"); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
<style>
.bg-interna{
	background: #e5e5e5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 171px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



		<!-- ======================================================================= -->
		<!-- Breadcrumbs    -->
		<!-- ======================================================================= -->
		<div class='container '>
			<div class="row">
				<div class="col-xs-12">
					<div class="breadcrumb top20">
						<a href="<?php echo Util::caminho_projeto(); ?>/mobile/"><i class="fa fa-home"></i></a>
						<a class="active">NOTÍCIAS</a>
					</div>
				</div>
			</div>
		</div>
		<!-- ======================================================================= -->
		<!-- Breadcrumbs    -->
		<!-- ======================================================================= -->



		<div class="container">
			<div class="row">
				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->
				<div class="col-xs-12 titulo_cat text-center top240">
					<h5>FIQUE POR DENTRO CONFIRA</h5>
					<h5><span>NOSSAS NOTÍCIAS</span></h5>

				</div>
				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->

			</div>
		</div>



		<div class="container">
			<div class="row">

					<!--  ==============================================================  -->
					<!--BARRA PESQUISAS-->
					<!--  ==============================================================  -->
					<div class="col-xs-12 top40">
						<form action="<?php echo Util::caminho_projeto() ?>/mobile/noticias/" method="post">
							<div class="input-group stylish-input-group">
								<input type="text" class="form-control" name="busca_noticias" placeholder="ENCONTRE A NOTÍCIA QUE DESEJA" >
								<span class="input-group-addon">
									<button type="submit">
										<span class="fa fa-search"></span>
									</button>
								</span>
							</div>
						</form>
					</div>
					<!--  ==============================================================  -->
					<!--BARRA PESQUISAS-->
					<!--  ==============================================================  -->

				</div>

			</div>
		</div>




		<div class="container bottom50">
			<div class="row">
				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->
				<?php

				//  FILTRA PELO TITULO BUSCA
				if(isset($_POST[busca_noticias]) and !empty($_POST[busca_noticias]) ):
					$complemento = "AND titulo LIKE '%$_POST[busca_noticias]%'";
				endif;


				$result = $obj_site->select("tb_noticias", $complemento);
				if(mysql_num_rows($result) == 0){
					echo "<h2 class='bg-info top25 ' style='padding: 20px; color:#000;'>Nenhum Notícia(s) encontrado.</h2>";
				}else{
					while ($row = mysql_fetch_array($result)) {
						?>

						<div class="col-xs-6 noticias_geral1 top40">

							<a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
								<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",534, 288, array("class"=>"input100 img-thumbnail", "alt"=>"$row[titulo]")) ?>
							</a>

							<div class="noticias_desc top15">
								<p>
									<?php Util::imprime($row[descricao],1000); ?>
								</p>
							</div>



						</div>
						<?php
						if ($i == 4) {
							echo '<div class="clearfix"></div>';
							$i = 0;
						}else{
							$i++;
						}
					}
				}
				?>
				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->

			</div>
		</div>

		<!-- ======================================================================= -->
		<!-- rodape    -->
		<!-- ======================================================================= -->
		<?php require_once('../includes/rodape.php') ?>
		<!-- ======================================================================= -->
		<!-- rodape    -->
		<!-- ======================================================================= -->


		</body>

		</html>

		<?php require_once("../includes/js_css.php"); ?>
