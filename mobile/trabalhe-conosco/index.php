
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once("../includes/head.php"); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20) ?>
<style>
.bg-interna{
	background: #e5e5e5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 171px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



		<!-- ======================================================================= -->
		<!-- Breadcrumbs    -->
		<!-- ======================================================================= -->
		<div class='container '>
			<div class="row">
				<div class="col-xs-12">
					<div class="breadcrumb top20">
						<a href="<?php echo Util::caminho_projeto(); ?>/mobile/"><i class="fa fa-home"></i></a>
						<a class="active">TRABALHE CONOSCO</a>
					</div>
				</div>
			</div>
		</div>
		<!-- ======================================================================= -->
		<!-- Breadcrumbs    -->
		<!-- ======================================================================= -->



		<div class="container">
			<div class="row">
				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->
				<div class="col-xs-12 titulo_cat text-center top240">
					<h5>FAÇA PARTE DA NOSSA EQUIPE</h5>
					<h5><span>TRABALHE CONOSCO</span></h5>

				</div>
				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->

			</div>
		</div>




		  <div class="container">
		    <div class="row top35">


					<!-- ======================================================================= -->
					<!-- CONTATOS   -->
					<!-- ======================================================================= -->
					<div class="col-xs-8  top20 contatos_home">
						<h1 class="bottom15">NOSSOS TELEFONES :</h1>



						<div class="media pull-right">
							<div class="media-left ">
								<h2><span class="right5"><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?></h2>
							</div>
							<div class="media-body">
								<div  class="media-heading">
									<img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/chamar_empresa.jpg" alt="">
								</div>
							</div>
						</div>

						<?php if (!empty($config[telefone2])): ?>
							<div class="media pull-right">
								<div class="media-left ">
									<h2><span class="right5"><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?></h2>
								</div>
								<div class="media-body">
									<div  class="media-heading">
										<img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/chamar_empresa.jpg" alt="">
									</div>
								</div>
							</div>
						<?php endif; ?>


					</div>
					<!-- ======================================================================= -->
					<!-- CONTATOS   -->
					<!-- ======================================================================= -->

					<div class="col-xs-12 text-right">
						<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/enviar_curriculo.png" alt="" />
					</div>

		    <div class="col-xs-12 padding0">

		      <!--  ==============================================================  -->
		      <!-- FORMULARIO-->
		      <!--  ==============================================================  -->
					<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
						<div class="fundo_formulario">
							<!-- formulario orcamento -->

								<div class="col-xs-12 top15">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
										<span class="fa fa-user form-control-feedback top15"></span>
									</div>
								</div>

								<div class="col-xs-12 top15">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
										<span class="fa fa-envelope form-control-feedback top15"></span>
									</div>
								</div>

								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
										<span class="fa fa-phone form-control-feedback top15"></span>
									</div>
								</div>

								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
										<span class="fa fa-book form-control-feedback "></span>
									</div>
								</div>


								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
										<span class="fa fa-suitcase form-control-feedback"></span>
									</div>
								</div>

								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback">
										<input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
										<span class="fa fa-briefcase form-control-feedback"></span>
									</div>
								</div>


								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
										<span class="fa fa-home form-control-feedback top15"></span>
									</div>
								</div>
								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback ">
										<input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
										<span class="fa fa-globe form-control-feedback top15"></span>
									</div>
								</div>


								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback ">
										<input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
										<span class="fa fa-file form-control-feedback top15"></span>
									</div>
								</div>



								<div class="col-xs-12 top15">
									<div class="form-group input100 has-feedback">
										<textarea name="mensagem" cols="30" rows="7" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
										<span class="fa fa-pencil form-control-feedback top20"></span>
									</div>
								</div>



							<!-- formulario orcamento -->
							<div class="col-xs-12 text-right">
								<div class="top15">
									<button type="submit" class="btn btn-formulario" name="btn_contato">
										ENVIAR
									</button>
								</div>
							</div>

						</div>

					</form>
		      <!--  ==============================================================  -->
		      <!-- FORMULARIO-->
		      <!--  ==============================================================  -->

		    </div>


		  </div>
		</div>



		<div class='container top10'>
			<div class="row relativo">

				<div class="saiba_mais_fale">
					<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba_mais_home.png" alt="" />
				</div>


				<!-- ======================================================================= -->
				<!-- SAIBA COMO CHEGAR   -->
				<!-- ======================================================================= -->
				<div class="col-xs-12 top20">
					<?php if (!empty($config[endereco])): ?>
						<div class="media bottom40">
							<div class="media-left media-middle ">
								<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_location.png" alt="">
							</div>
							<div class="media-body">
								<p class="media-heading">
									<span class="right5"><?php Util::imprime($config[endereco]) ?>
								</p>
							</div>
						</div>
					<?php endif; ?>

					<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="577" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<!-- ======================================================================= -->
				<!-- SAIBA COMO CHEGAR   -->
				<!-- ======================================================================= -->


			</div>
		</div>




		<!-- ======================================================================= -->
		<!-- rodape    -->
		<!-- ======================================================================= -->
		<?php require_once('../includes/rodape.php') ?>
		<!-- ======================================================================= -->
		<!-- rodape    -->
		<!-- ======================================================================= -->


		</body>

		</html>

		<?php require_once("../includes/js_css.php"); ?>



		<?php
		//  VERIFICO SE E PARA ENVIAR O EMAIL
		if(isset($_POST[nome]))
		{

		  if(!empty($_FILES[curriculo][name])):
		    $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
		    $texto = "Anexo: ";
		    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
		    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
		  endif;

		  $texto_mensagem = "
		  Nome: ".$_POST[nome]." <br />
		  Telefone: ".$_POST[telefone]." <br />
		  Email: ".$_POST[email]." <br />
		  Escolaridade: ".$_POST[escolaridade]." <br />
		  Cargo: ".$_POST[cargo]." <br />
		  Área: ".$_POST[area]." <br />
		  Cidade: ".$_POST[cidade]." <br />
		  Estado: ".$_POST[estado]." <br />
		  Mensagem: <br />
		  ".nl2br($_POST[mensagem])."

		  <br><br>
		  $texto
		  ";

			if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
 			 Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou curriculo pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
 			 Util::alert_bootstrap("Obrigado por entrar em contato.");
 			 unset($_POST);
 		 }else{
 			 Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
 		 }

 	 }

		?>




		<script>
		$(document).ready(function() {
		  $('.FormContatos').bootstrapValidator({
		    message: 'This value is not valid',
		    feedbackIcons: {
		      valid: 'fa fa-check',
		      invalid: 'fa fa-remove',
		      validating: 'fa fa-refresh'
		    },
		    fields: {
		      nome: {
		        validators: {
		          notEmpty: {
		            message: 'Informe nome.'
		          }
		        }
		      },
		      email: {
		        validators: {
		          notEmpty: {
		            message: 'insira email.'
		          },
		          emailAddress: {
		            message: 'Esse endereço de email não é válido'
		          }
		        }
		      },
		      telefone: {
		        validators: {
		          notEmpty: {
		            message: 'Por favor adicione seu numero.'
		          },
		          phone: {
		            country: 'BR',
		            message: 'Informe um telefone válido.'
		          }
		        },
		      },
		      assunto: {
		        validators: {
		          notEmpty: {

		          }
		        }
		      },
		      cidade: {
		        validators: {
		          notEmpty: {

		          }
		        }
		      },
		      estado: {
		        validators: {
		          notEmpty: {

		          }
		        }
		      },
		      escolaridade1: {
		        validators: {
		          notEmpty: {

		          }
		        }
		      },
		      curriculo: {
		        validators: {
		          notEmpty: {
		            message: 'Por favor insira seu currículo'
		          },
		          file: {
		            extension: 'doc,docx,pdf,rtf',
		            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
		            maxSize: 5*1024*1024,   // 5 MB
		            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
		          }
		        }
		      },
		      mensagem: {
		        validators: {
		          notEmpty: {

		          }
		        }
		      }
		    }
		  });
		});
		</script>
