
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once("../includes/head.php"); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
<style>
.bg-interna{
	background: #e5e5e5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 171px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->



	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12">
				<div class="breadcrumb top20">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/"><i class="fa fa-home"></i></a>
					<a class="active">PRODUTOS</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->



	<div class="container">
		<div class="row top25">
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->
			<div class="col-xs-6 titulo_cat_produtos">
				<h5>CONFIRA TODOS OS </h5>
				<h5><span>NOSSOS PRODUTOS</span></h5>
			</div>
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->


			<!--  ==============================================================  -->
			<!--BARRA PESQUISAS-->
			<!--  ==============================================================  -->
			<div class="col-xs-6 barra_pesquisa">
				<form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
					<div class="input-group stylish-input-group">
						<input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" >
						<span class="input-group-addon">
							<button type="submit">
								<span class="fa fa-search"></span>
							</button>
						</span>
					</div>
				</form>
			</div>
			<!--  ==============================================================  -->
			<!--BARRA PESQUISAS-->
			<!--  ==============================================================  -->


			<!--  ==============================================================  -->
			<!--menu categoria produto-->
			<!--  ==============================================================  -->
			<?php require_once('../includes/menu_produtos_categorias.php') ?>
			<!--  ==============================================================  -->
			<!--menu categoria produto-->
			<!--  ==============================================================  -->
		</div>
	</div>





	<div class="container bottom50">
		<div class="row">
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->
			<?php

			//  FILTRA PELO TITULO BUSCA
			if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
				$complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
			endif;


			$ids = explode("/", $_GET[get1]);


			//  FILTRA AS CATEGORIAS
			if (!empty( $ids[0] )) {
				$id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $ids[0]);
				$complemento .= "AND id_categoriaproduto = '$id_categoria' ";
			}


			//  FILTRA AS SUBCATEGORIAS
			if (isset( $ids[1] )) {
				$id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $ids[1]);
				$complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
			}

			//  FILTRA PELO TITULO CATEGORIAS
			if(isset($_POST[id_categoriaproduto]) and !empty($_POST[id_categoriaproduto])):
				$complemento .= "AND id_categoriaproduto = '$_POST[id_categoriaproduto]' ";
			endif;

			//  FILTRA PELO TITULO SUBCATEGORIAS
			if(isset($_POST[id_subcategoriaproduto]) and !empty($_POST[id_subcategoriaproduto]) ):
				$complemento .= "AND id_subcategoriaproduto = '$_POST[id_subcategoriaproduto]' ";
			endif;





			$result = $obj_site->select("tb_produtos", $complemento);
			if(mysql_num_rows($result) == 0){
				echo "<h2 class='bg-info top25 ' style='padding: 20px; color:#000;'>Nenhum Produto(s) encontrado.</h2>";
			}else{
				while ($row = mysql_fetch_array($result)) {
					?>
					<div class="col-xs-6 produtos_cat top10">

						<div class="thumbnail">
							<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">

								<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 190, 198, array("class"=>"top5", "alt"=>"$row[titulo]") ) ?>

							</a>
							<div class="caption">
								<h1><?php Util::imprime($row[titulo]) ?></h1>
								<!-- <div class="top10">

								<p><?php //Util::imprime($row[descricao], 300) ?></p>

							</div> -->
							<div class="top15 font-futura">
								<?php if ($row[pode_ser_vendido] == 'SIM'): ?>
					                <a class="btn btn_saiba_mais_produtos right10" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" data-toggle="tooltip"  title="Comprar">
					                  COMPRAR
					                </a>
					            <?php endif; ?>

					            <?php if ($row[pode_ser_alugado] == 'SIM'): ?>
					            <a class="btn btn_saiba_mais_produtos" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'"   data-toggle="tooltip"  title="Alugar">
					              ALUGAR
					            </a>
					            <?php endif; ?>
							</div>
							<a class="btn btn_saiba_mais_home top10" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]) ?>"  title="Saiba Mais">
								SAIBA MAIS <i class="fa fa-angle-right left10"></i>
							</a>
						</div>
					</div>


				</div>
				<?php
				if ($i == 3) {
					echo '<div class="clearfix"></div>';
					$i = 0;
				}else{
					$i++;
				}
			}
		}
		?>
		<!-- ======================================================================= -->
		<!-- TITULO BANNER E DESCRICAO -->
		<!-- ======================================================================= -->

	</div>
</div>

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>
