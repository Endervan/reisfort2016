<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  120px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">A EMPRESA</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->
      <div class="col-xs-7 titulo_cat top50">
        <h5>CONHEÇA MELHOR</h5>
        <h5><span>A REISFORTS </span></h5>
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="top45 scrol_empresa">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->

    </div>
  </div>






  <div class='container top10'>
    <div class="row">
      <!-- ======================================================================= -->
      <!-- SAIBA COMO CHEGAR   -->
      <!-- ======================================================================= -->
      <div class="col-xs-8">
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="577" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <!-- ======================================================================= -->
      <!-- SAIBA COMO CHEGAR   -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- CONTATOS   -->
      <!-- ======================================================================= -->
      <div class="col-xs-4  top10 contatos_home">
        <h1>NOSSOS TELEFONES :</h1>



        <div class="media">
          <div class="media-left media-middle ">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone1.png" alt="">
          </div>
          <div class="media-body">
            <h2 class="media-heading">
              <span class="right5"><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
            </h2>
          </div>
        </div>

        <?php if (!empty($config[telefone2])): ?>
          <div class="media">
            <div class="media-left media-middle ">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone1.png" alt="">
            </div>
            <div class="media-body">
              <h2 class="media-heading">
                <span class="right5"><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
              </h2>
            </div>
          </div>
        <?php endif; ?>


        <?php if (!empty($config[endereco])): ?>
          <div class="media">
            <div class="media-left media-middle ">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_location.png" alt="">
            </div>
            <div class="media-body">
              <p class="media-heading">
                <span class="right5"><?php Util::imprime($config[endereco]) ?>
              </p>
            </div>
          </div>
        <?php endif; ?>




        <div class="top40">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba_como_chegar.png" alt="">
        </div>



      </div>
      <!-- ======================================================================= -->
      <!-- CONTATOS   -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
