<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row tendas relativo">
      <!-- ======================================================================= -->
      <!-- NOSSAS TENDAS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-2">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/tendas.png" alt="" />
      </div>
      <div class="col-xs-10 lato-black top50 padding0">
        <h3>NOSSAS TENDAS</h3>
      </div>
      <div class="barra_home"></div>
      <!-- ======================================================================= -->
      <!-- NOSSAS TENDAS    -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- slider produtos    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 padding0">
        <?php require_once('./includes/slider_produtos.php'); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- slider produtos    -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <div class="container top100">
    <div class="row tendas relativo">
      <!-- ======================================================================= -->
      <!-- NOSSOS BANHEIROS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-2">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/banheiros.png" alt="" />
      </div>
      <div class="col-xs-10 lato-black top50 padding0">
        <h3>NOSSOS BANHEIROS QUÍMICOS</h3>
      </div>
      <div class="barra_home"></div>
      <!-- ======================================================================= -->
      <!-- NOSSOS BANHEIROS    -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- slider produtos    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 padding0">
        <?php require_once('./includes/slider_produtos_banheiros.php'); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- slider produtos    -->
      <!-- ======================================================================= -->

    </div>
  </div>




  <div class="container-fluid top100">
    <div class="row bg_empresa_home">

      <div class="container">
        <div class="row">
          <!-- ======================================================================= -->
          <!-- EMPRESA HOME    -->
          <!-- ======================================================================= -->
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

          <div class="col-xs-7">
            <div class="conheca_home top40">
              <h1>CONHEÇA MAIS A <span><?php Util::imprime($row[titulo]); ?></span></h1>
            </div>
            <div class="conheca_mais_home top20">
              <p><?php Util::imprime($row[descricao],1000); ?></p>
            </div>

            <div class="col-xs-4 padding0 top15">
              <a class="btn btn_conheca_mais col-xs-12" href="<?php echo Util::caminho_projeto() ?>/empresa"  title="SAIBA MAIS">SAIBA MAIS</a>
            </div>

          </div>
          <div class="clearfix"></div>
          <!-- ======================================================================= -->
          <!-- EMPRESA HOME    -->
          <!-- ======================================================================= -->

          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->
          <div class="col-xs-4 text-center contatos_home lato_bold top280">

            <div class="media">
              <div class="media-left media-middle ">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone.png" alt="">
              </div>
              <div class="media-body">
                <h2 class="media-heading">
                  <span class="right5"><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
                </h2>
              </div>
            </div>

            <?php if (!empty($config[telefone2])): ?>
              <div class="media">
                <div class="media-left media-middle ">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone.png" alt="">
                </div>
                <div class="media-body">
                  <h2 class="media-heading">
                    <span class="right5"><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
                  </h2>
                </div>
              </div>
            <?php endif; ?>

          </div>
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->
          <!--  ==============================================================  -->
          <!-- MAPA -->
          <!--  ==============================================================  -->
          <div class="col-xs-8">
            <?php if (!empty($config[src_place])): ?>
              <div class="effect2">
                <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="402" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
            <?php endif; ?>
          </div>
          <!--  ==============================================================  -->
          <!-- MAPA -->
          <!--  ==============================================================  -->
        </div>
      </div>
    </div>
  </div>




  <div class="container">
    <div class="row top105">
      <!-- ======================================================================= -->
      <!--NOSSAS NOTICIAS    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12">
        <h6>NOSSAS <span>NOTÍCIAS</span></h6>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_personalizavel.png" alt="" class="input100"/>
      </div>

      <?php
      $result = $obj_site->select("tb_noticias","order by rand() limit 3");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
          ?>

          <div class="col-xs-4 ">
            <div class="relativo">
              <div class="col-xs-8 bg_cinza pb10">
                <div class="top15 dicas_descricao_home pr40">
                  <p>
                    <?php Util::imprime($row[descricao],1000); ?>
                  </p>
                </div>
                <a class="btn btn_dicas_home col-xs-8  top20" href="<?php echo Util::caminho_projeto() ?>/noticia/<?php echo Util::imprime($row[url_amigavel]); ?>"  title="SAIBA MAIS">SAIBA MAIS</a>
              </div>
              <div class="img_dicas">
                <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",168, 160, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                </a>
              </div>
            </div>
          </div>
          <?php
          if ($i == 2) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>

      <!-- ======================================================================= -->
      <!--NOSSAS NOTICIAS    -->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!--pagamentos home    -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 top40 bottom80">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/formas_pagamentos_home.jpg" alt="" class="input100"/>
      </div>
      <!-- ======================================================================= -->
      <!--pagamentos home    -->
      <!-- ======================================================================= -->



    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    transitionType: 'move',
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
