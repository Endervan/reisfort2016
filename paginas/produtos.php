<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #e5e5e5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  120px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">PRODUTOS</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->


  <div class='container '>
    <div class="row bottom50">

      <!-- ======================================================================= -->
      <!-- MENU LATERAL PRODUTOS   -->
      <!-- ======================================================================= -->
      <div class="col-xs-3 top50">
        <?php require_once('./includes/menu_lateral.php') ?>
      </div>

      <!-- ======================================================================= -->
      <!-- MENU LATERAL PRODUTOS   -->
      <!-- ======================================================================= -->


      <div class="col-xs-9 padding0 top50">
        <!-- ======================================================================= -->
        <!-- titulo produtos   -->
        <!-- ======================================================================= -->

        <div class="col-xs-6">
          <h4>CONFIRA TODOS OS <span>NOSSOS PRODUTOS</span></h4>
        </div>
        <!-- ======================================================================= -->
        <!-- titulo produtos   -->
        <!-- ======================================================================= -->





        <div class="col-xs-7 top20">
          <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
            <div class="input-group stylish-input-group">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

                <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA" >
                <span class="input-group-addon">
                  <button type="submit">
                    <span class="fa fa-search"></span>
                  </button>
                </span>

              </form>

            </div>
            <!--  ==============================================================  -->
            <!--BARRA PESQUISAS-->
            <!--  ==============================================================  -->
          </div>

          <!--  ==============================================================  -->
          <!-- BARRA CATEGORIA ORDENA POR-->
          <!--  ==============================================================  -->
          <div class="col-xs-5 top20 barra_categoria">
            <form name="form_ordem" action="" method="post">
              <select name="ordem_produtos" class="selectpicker" data-live-search="false" title="ORDENAR POR" data-width="100%" onchange="this.form.submit()" >
                <optgroup label="SELECIONE">
                      <option value="mais-recentes" <?php if($_POST[ordem_produtos] == "mais-recentes"){ echo "selected"; } ?> data-tokens="MAIS RECENTES">MAIS RECENTES</option>
                      <option value="mais-vistos" <?php if($_POST[ordem_produtos] == "mais-vistos"){ echo "selected"; } ?> data-tokens="MAIS VISTOS">MAIS VISTOS</option>
                  </optgroup>
                </select>
              </form>
          </div>
          <!--  ==============================================================  -->
          <!-- BARRA CATEGORIA ORDENA POR-->
          <!--  ==============================================================  -->

          <div class="clearfix"></div>


          <div class="lista_parceiros_produtos">

            <?php

            $url1 = Url::getURL(1);
            $url2 = Url::getURL(2);

            //  FILTRA AS CATEGORIAS
            if (isset( $url1 )) {
              $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
              $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
            }


            //  FILTRA AS SUBCATEGORIAS
            if (isset( $url2 )) {
              $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $url2);
              $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
            }



            //  FILTRA PELO TITULO CATEGORIAS
            if(isset($_POST[id_categoriaproduto]) and !empty($_POST[id_categoriaproduto])):
              $complemento .= "AND id_categoriaproduto = '$_POST[id_categoriaproduto]' ";
            endif;

            //  FILTRA PELO TITULO SUBCATEGORIAS
            if(isset($_POST[id_subcategoriaproduto]) and !empty($_POST[id_subcategoriaproduto]) ):
              $complemento .= "AND id_subcategoriaproduto = '$_POST[id_subcategoriaproduto]' ";
            endif;


            //  FILTRA PELO TITULO
            if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
              $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
            endif;

            // ORDEM DOS PRODUTOS
            switch ($_POST[ordem_produtos]) {
              case 'mais-vistos':
              $complemento .= "order by qtd_visitas desc";
              break;
              case 'mais-recentes':
              $complemento .= "order by idproduto desc";
              break;


            }


            $result = $obj_site->select("tb_produtos", $complemento);

            //  LISTA OS PRODUTOS
            require_once("./includes/lista_produtos.php");
            ?>

          </div>




        </div>
      </form>



    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
