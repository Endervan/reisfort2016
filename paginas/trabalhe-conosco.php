<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  128px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">TRABALHE CONOSCO</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->
      <div class="col-xs-10 titulo_orcamento top35">
        <h5><span>FAÇA PARTE DA NOSSA EQUIPE </span>TRABALHE CONOSCO</h5>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->

    </div>
  </div>





  <div class="container">
    <div class="row top35">

      <!-- ======================================================================= -->
      <!-- CONTATOS   -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 padding0 contatos_home">
        <h1 class="left15">TELEFONES :</h1>


        <div class="media col-xs-3">
          <div class="media-left media-middle ">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone1.png" alt="">
          </div>
          <div class="media-body">
            <h2 class="media-heading ">
              <span class="right5"><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
            </h2>
          </div>
        </div>

        <?php if (!empty($config[telefone2])): ?>
          <div class="media col-xs-3 padding0">
            <div class="media-left media-middle ">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone1.png" alt="">
            </div>
            <div class="media-body">
              <h2 class="media-heading">
                <span class="right5"><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
              </h2>
            </div>
          </div>
        <?php endif; ?>

      </div>
      <!-- ======================================================================= -->
      <!-- CONTATOS   -->
      <!-- ======================================================================= -->

    </div>

    <div class="col-xs-4">
      <div class="top80">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/envia_email.png" alt="" />
      </div>
    </div>
    <div class="col-xs-8 top10">
      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
      <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
        <div class="fundo_formulario">
          <!-- formulario orcamento -->
          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                <span class="fa fa-book form-control-feedback "></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>




          <div class="top20">

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                <span class="fa fa-suitcase form-control-feedback"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback">
                <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                <span class="fa fa-briefcase form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="clearfix"></div>

          <div class="top20">

            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                <span class="fa fa-home form-control-feedback top15"></span>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback ">
                <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                <span class="fa fa-globe form-control-feedback top15"></span>
              </div>
            </div>

          </div>


          <div class="clearfix"></div>

          <div class="top20">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback ">
                <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                <span class="fa fa-file form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="30" rows="7" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback top20"></span>
              </div>
            </div>
          </div>


          <!-- formulario orcamento -->
          <div class="col-xs-12 text-right">
            <div class="top15">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>

      </form>
    </div>
  </div>

  <!--  ==============================================================  -->
  <!-- FORMULARIO-->
  <!--  ==============================================================  -->

</div>


</div>
</div>



<!-- ======================================================================= -->
<!-- CONTATOS   -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">

    <div class="col-xs-8 mapa_trabalhe">
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="870" frameborder="0" style="border:0" allowfullscreen></iframe>

      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
    </div>

    <div class="col-xs-4 text-center top105">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba_como_chegar.png" alt="">
    </div>

    <div class="col-xs-12  top10 bottom40 contatos_home">

      <?php if (!empty($config[endereco])): ?>
        <div class="media">
          <div class="media-left media-middle ">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_location.png" alt="">
          </div>
          <div class="media-body">
            <p class="media-heading">
              <span class="right5"><?php Util::imprime($config[endereco]) ?>
              </p>
            </div>
          </div>
        <?php endif; ?>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CONTATOS   -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Email: ".$_POST[email]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Cargo: ".$_POST[cargo]." <br />
  Área: ".$_POST[area]." <br />
  Cidade: ".$_POST[cidade]." <br />
  Estado: ".$_POST[estado]." <br />
  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";



    Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), ($_POST[email]));
    Util::alert_bootstrap("Obrigado por entrar em contato.");
    unset($_POST);
  }

?>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
