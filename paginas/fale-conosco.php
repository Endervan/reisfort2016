<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",9) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  128px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">FALE CONOSCO</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->
      <div class="col-xs-10 titulo_orcamento top35">
        <h5><span>ESTAMOS PRONTOS </span>PARA ATENDÊ-LO</h5>
      </div>
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->

    </div>
  </div>





  <div class="container">
    <div class="row top35">

      <!-- ======================================================================= -->
      <!-- CONTATOS   -->
      <!-- ======================================================================= -->
      <div class="col-xs-12 padding0 contatos_home">
        <h1 class="left15">NOS TELEFONES :</h1>


        <div class="media col-xs-3">
          <div class="media-left media-middle ">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone1.png" alt="">
          </div>
          <div class="media-body">
            <h2 class="media-heading ">
              <span class="right5"><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
            </h2>
          </div>
        </div>

        <?php if (!empty($config[telefone2])): ?>
          <div class="media col-xs-3 padding0">
            <div class="media-left media-middle ">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_phone1.png" alt="">
            </div>
            <div class="media-body">
              <h2 class="media-heading">
                <span class="right5"><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
              </h2>
            </div>
          </div>
        <?php endif; ?>

      </div>
      <!-- ======================================================================= -->
      <!-- CONTATOS   -->
      <!-- ======================================================================= -->

    </div>

    <div class="col-xs-4">
      <div class="top105">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/envia_email.png" alt="" />
      </div>
    </div>
    <div class="col-xs-8 top10">

      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->
      <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
        <div class="fundo_formulario">
          <!-- formulario orcamento -->
          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>


          <div class="top20">
            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                <span class="fa fa-star form-control-feedback top15"></span>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>



          <div class="top15">
            <div class="col-xs-12">
              <div class="form-group input100 has-feedback">
                <textarea name="mensagem" cols="25" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback top15"></span>
              </div>
            </div>
          </div>


          <div class="col-xs-12 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>
      </form>
      <!--  ==============================================================  -->
      <!-- FORMULARIO-->
      <!--  ==============================================================  -->

    </div>


  </div>
</div>

</form>




<!-- ======================================================================= -->
<!-- CONTATOS   -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">

    <div class="col-xs-8 mapa">
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="570" frameborder="0" style="border:0" allowfullscreen></iframe>

      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
    </div>

    <div class="col-xs-4 text-center top105">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba_como_chegar.png" alt="">
    </div>

    <div class="col-xs-12  top10 bottom40 contatos_home">

      <?php if (!empty($config[endereco])): ?>
        <div class="media">
          <div class="media-left media-middle ">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_location.png" alt="">
          </div>
          <div class="media-body">
            <p class="media-heading">
              <span class="right5"><?php Util::imprime($config[endereco]) ?>
              </p>
            </div>
          </div>
        <?php endif; ?>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CONTATOS   -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
                        {
                          $texto_mensagem = "
                          Nome: ".($_POST[nome])." <br />
                          Telefone: ".($_POST[telefone])." <br />
                          Email: ".($_POST[email])." <br />
                          Assunto: ".($_POST[assunto])." <br />
                          Mensagem: <br />
                          ".(nl2br($_POST[mensagem]))."
                          ";


                          if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
                            Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
                            Util::alert_bootstrap("Obrigado por entrar em contato.");
                            unset($_POST);
                          }else{
                            Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
                          }

                        }


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
