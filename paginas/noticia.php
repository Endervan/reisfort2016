<?php
// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_noticias", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/noticias");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: #e5e5e5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  120px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a href="<?php echo Util::caminho_projeto(); ?>/">NOTÍCIAS</i></a>
          <a class="active"><?php Util::imprime($dados_dentro[titulo]); ?></a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->
      <div class="col-xs-8 titulo_cat top50">
        <h5>FIQUE POR DENTRO</h5>
        <h5><span>CONFIRA NOSSAS NOTÍCIAS</span></h5>

        <!--  ==============================================================  -->
        <!--BARRA PESQUISAS-->
        <!--  ==============================================================  -->
        <div class="col-xs-11 padding0">
          <form action="<?php echo Util::caminho_projeto() ?>/noticias/" method="post">
            <div class="input-group stylish-input-group">
              <input type="text" class="form-control" name="busca_noticias" placeholder="ENCONTRE A NOTÍCIA QUE DESEJA" >
              <span class="input-group-addon">
                <button type="submit">
                  <span class="fa fa-search"></span>
                </button>
              </span>
            </div>
          </form>
        </div>
        <!--  ==============================================================  -->
        <!--BARRA PESQUISAS-->
        <!--  ==============================================================  -->

      </div>
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->

    </div>
  </div>




    <div class='container '>
      <div class="row">
        <div class="col-xs-12 top40">

          <!-- ======================================================================= -->
          <!-- PRODUTOS DENTRO DESC -->
          <!-- ======================================================================= -->
          <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",1130, 399, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>

            <div class="top20">
              <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
            </div>
            <div class="top40">
              <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
            </div>

                <!-- items mirrored twice, total of 12 -->
              </ul>
            </div>
            <!-- ======================================================================= -->
            <!-- PRODUTOS DENTRO DESC -->
            <!-- ======================================================================= -->

        </div>
      </div>



        <div class="container bottom50">
          <div class="row">
            <div class="col-xs-12 top40">
              <h6>VEJA <span>TAMBÉM</span></h6>
            </div>
            <!-- ======================================================================= -->
            <!-- TITULO BANNER E DESCRICAO -->
            <!-- ======================================================================= -->
            <?php

            //  FILTRA PELO TITULO BUSCA
            if(isset($_POST[busca_noticias]) and !empty($_POST[busca_noticias]) ):
              $complemento = "AND titulo LIKE '%$_POST[busca_noticias]%'";
            endif;



           $result = $obj_site->select("tb_noticias","order by rand() limit 2", $complemento);
           if(mysql_num_rows($result) == 0){
              echo "<h2 class='bg-info top25 ' style='padding: 20px; color:#000;'>Nenhum Notícia(s) encontrado.</h2>";
           }else{
             while ($row = mysql_fetch_array($result)) {
             ?>

                <div class="col-xs-6 noticias_geral top40">

                  <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",534, 288, array("class"=>"input100 img-thumbnail", "alt"=>"$row[titulo]")) ?>
                  </a>

                  <div class="noticias_desc top15">
                    <p>
                      <?php Util::imprime($row[descricao],1000); ?>
                    </p>
                  </div>

                  <div class="produto-hover">

                    <div class="col-xs-12 text-center hover-btn-orcamento">
                      <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip"  title="Saiba mais">
                        <i class="fa fa-plus-circle fa-5x"></i>
                      </a>
                    </div>
                  </div>

                </div>
                <?php
                if ($i == 3) {
                  echo '<div class="clearfix"></div>';
                  $i = 0;
                }else{
                  $i++;
                }
              }
            }
            ?>
            <!-- ======================================================================= -->
            <!-- TITULO BANNER E DESCRICAO -->
            <!-- ======================================================================= -->

          </div>
        </div>






  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>
</html>
<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(document).ready(function() {

  $(".produto-hover").hover(
    function () {
      $(this).stop().animate({opacity:1});
    },
    function () {
      $(this).stop().animate({opacity:0});
    }
  );

});
</script>
