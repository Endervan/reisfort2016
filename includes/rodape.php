<div class="clearfix"></div>
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>
		<div class="container">
			<div class="row">



				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-12 padding0">

					<!-- ======================================================================= -->
					<!-- LOGO    -->
					<!-- ======================================================================= -->
					<div class="col-xs-4 top5">
						<a href="<?php echo Util::caminho_projeto() ?>/">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="" />
						</a>
					</div>
					<!-- ======================================================================= -->
					<!-- LOGO    -->
					<!-- ======================================================================= -->
					<div class="col-xs-8 top45">

						<ul class="menu-rodape">
							<li>
								<a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">
									HOME
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/empresa">
									A EMPRESA
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "produtos-categoria" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos">
									PRODUTOS
								</a>
							</li>


							<li>
								<a class="<?php if(Url::getURL( 0 ) == "noticias" or Url::getURL( 0 ) == "noticia"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/noticias">
									NOTÍCIAS
								</a>
							</li>

							<li>
								<a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
									FALE CONOSCO
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
									TRABALHE CONOSCO
								</a>
							</li>

						</ul>

					</div>
					<div class="col-xs-12"><div class="barra_branca"></div></div>
				</div>
				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->





				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="col-xs-10 top20 telefone_rodape">
					<p class="bottom15"><i class="fa fa-home right10"></i><?php Util::imprime($config[endereco]); ?></p>
					<p class="bottom15">
						<i class="fa fa-phone right10"></i>
						<?php Util::imprime($config[ddd1]); ?>
						<?php Util::imprime($config[telefone1]); ?>

						<?php if (!empty($config[telefone2])) { ?>
							<span class="left15"></span>
							<?php Util::imprime($config[ddd2]); ?>
							<?php Util::imprime($config[telefone2]); ?>
						<?php } ?>

						<?php if (!empty($config[telefone3])) { ?>
							<span class="left20">VENDAS - </span>
							<?php Util::imprime($config[ddd3]); ?>
							<?php Util::imprime($config[telefone3]); ?>
						<?php } ?>

						<?php if (!empty($config[telefone4])) { ?>
							<span class="left20">LOCAÇÕES - </span>
							<?php Util::imprime($config[ddd4]); ?>
							<?php Util::imprime($config[telefone4]); ?>
						<?php } ?>

						</p>
					</div>
					<!-- ======================================================================= -->
					<!-- ENDERECO E TELEFONES    -->
					<!-- ======================================================================= -->


					<div class="col-xs-2 text-right top25">
						<?php if ($config[google_plus] != "") { ?>
							<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
								<i class="fa fa-google-plus right15"></i>
							</a>
							<?php } ?>

							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>






		<div class="container-fluid">
			<div class="row rodape-preto">
				<div class="col-xs-12 text-center top15 bottom15">
					<h5>© Copyright REISFORTS TENDAS E BANHEIROS QUÍMICOS</h5>
				</div>
			</div>
		</div>
