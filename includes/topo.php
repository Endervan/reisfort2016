

<div class="container-fluid topo">
  <div class="row">

    <div class="container">
      <div class="row">
        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->
        <div class="col-xs-4 div_personalizada text-center top20">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="HOME">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
          </a>
        </div>

        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->

        <div class="col-xs-8 padding0">
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->


          <div class="col-xs-9 top5 padding0 lato_bold telefone_topo">

            <div class="col-xs-4 padding0">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone_telefone.png" alt="">
            </div>
            <div class="col-xs-8 padding0">
              <?php if (!empty($config[telefone3])): ?>
                <div class="col-xs-6 padding0">

                  <h6> VENDAS</h6>
                  <h2 class=""><span><?php Util::imprime($config[ddd3]) ?></span><?php Util::imprime($config[telefone3]) ?></h2>

                </div>
              <?php endif; ?>


              <div class="col-xs-6 padding0">
                <h6>LOCAÇÃO </h6>
                <h2><span ><?php Util::imprime($config[ddd4]) ?></span><?php Util::imprime($config[telefone4]) ?></h2>
              </div>


            </div>


          </div>
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->

          <!--  ==============================================================  -->
          <!--CARRINHO-->
          <!--  ==============================================================  -->
          <div class="col-xs-3 dropdown top10 padding0">
            <a class="btn btn_carrinho" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/carrinho_topo.jpg" alt="" />
            </a>


            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">

              <!--  ==============================================================  -->
              <!--sessao adicionar produtos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <div class="lista-itens-carrinho top15 bottom15">
                    <div class="col-xs-2">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1 top10">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                    </div>
                    <div class="col-xs-12 bottom5"><div class="borda_carrinho top5">  </div> </div>
                  </div>
                  <?php
                }
              }
              ?>


              <!--  ==============================================================  -->
              <!--sessao adicionar servicos-->
              <!--  ==============================================================  -->
              <?php
              if(count($_SESSION[solicitacoes_servicos]) > 0)
              {
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                    <div class="col-xs-2">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1 top10">
                      <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                    </div>
                    <div class="col-xs-12 bottom5"><div class="borda_carrinho top5">  </div> </div>

                  </div>
                  <?php
                }
              }
              ?>




              <div class="col-xs-12 text-right top10 bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_azul" >
                  ENVIAR ORÇAMENTO
                </a>
              </div>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--CARRINHO-->
          <!--  ==============================================================  -->



          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->
          <div class="col-xs-12 menu">
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
              <ul class="nav navbar-nav navbar-right lato_bold">
                <li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
                </li>
                <li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "produtos-categoria" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
                </li>


                <li class="<?php if(Url::getURL( 0 ) == "noticias" or Url::getURL( 0 ) == "noticia"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/noticias">NOTÍCIAS</a>
                </li>


                <li class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
                </li>

              </ul>

            </div><!--/.nav-collapse -->
          </div>
          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->

        </div>

      </div>
    </div>
  </div>
</div>

<?php /*


*/ ?>
