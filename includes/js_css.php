
<link rel="icon" href="<?php echo Util::caminho_projeto() ?>/imgs/favicon.ico">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="<?php echo Util::caminho_projeto(); ?>/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<!-- Custom styles for this template -->
<link href="<?php echo Util::caminho_projeto() ?>/css/non-responsive.css" rel="stylesheet">

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="<?php echo Util::caminho_projeto() ?>/css/normalize.css" rel="stylesheet">
<link href="<?php echo Util::caminho_projeto() ?>/css/normalize.min.css.map" rel="stylesheet">

<script src="<?php echo Util::caminho_projeto() ?>/js/ie10-viewport-bug-workaround.js"></script>


<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrapValidator.min.css"/>


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-lightbox.min.js"></script>


<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
  window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>





<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/js/jquery.flexslider-min.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shBrushJScript.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/demo.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.js"></script>

<script defer src="<?php echo Util::caminho_projeto() ?>/js/block-slider.min.js"></script>




<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/flexslider.css" type="text/css" media="screen" />




<!-- - LAYER SLIDER - -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/touchcarousel.css"/>
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/black-and-white-skin.css" />
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.touchcarousel-1.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  $("#carousel-gallery").touchCarousel({
    itemsPerPage: 1,
    scrollbar: true,
    scrollbarAutoHide: true,
    scrollbarTheme: "dark",
    pagingNav: false,
    snapToItems: true,
    scrollToLast: false,
    useWebkit3d: true,
    loopItems: true,
    autoplay: true
  });
});
</script>
<!-- XXXX LAYER SLIDER XXXX -->




<!--    ====================================================================================================     -->
<!--    CLASSE DE FONTS DO SITE   -->
<!--    ====================================================================================================     -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/fonts/font-awesome-4.6.3/css/font-awesome.min.css">


<!-- ======================================================================= -->
<!-- USA SETA UP  USE RODAPE -->
<!-- ======================================================================= -->
<script type="text/javascript">
$(document).ready(function(){

  $(window).scroll(function(){
    if ($(this).scrollTop() > 800) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });

  $('.scrollup').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 800);
    return false;
  });

});
</script>

<!--jQuery GEREALMENTE JA ESTA HEAD VERIFICAR-->

<!-- ======================================================================= -->
<!-- USA SETA UP   -->
<!-- ======================================================================= -->



<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 4
});

</script>
<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- scroll personalizado    -->
<!-- ======================================================================= -->

<!-- the mousewheel plugin - optional to provide mousewheel support -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.min.js"></script>

<!-- the jScrollPane script -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/jquery.jscrollpane.min.js"></script>


<script type="text/javascript" >
$(function() {
  $('.lista_produtos .list-group').jScrollPane();
});

$(function() {
  $('.scrol_empresa').jScrollPane();
});
</script>
<!-- ======================================================================= -->
<!-- scroll personalizado    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- colobox    -->
<!-- ======================================================================= -->

<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/js/colorbox/example1/colorbox.css" />
<script src="<?php echo Util::caminho_projeto() ?>/js/colorbox/jquery.colorbox-min.js"></script>
<script>
$(document).ready(function(){
  //Examples of how to assign the Colorbox event to elements
  $(".group1").colorbox({rel:'group1'});
  $(".group2").colorbox({rel:'group2', transition:"fade"});
  $(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
  $(".group4").colorbox({rel:'group4', slideshow:true});
  $(".ajax").colorbox();
  $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
  $(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
  $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
  $(".inline").colorbox({inline:true, width:"50%"});
  $(".callbacks").colorbox({
    onOpen:function(){ alert('onOpen: colorbox is about to open'); },
    onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
    onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
    onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
    onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
  });

  $('.non-retina').colorbox({rel:'group5', transition:'none'})
  $('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});

  //Example of preserving a JavaScript event for inline calls.
  $("#click").click(function(){
    $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
    return false;
  });
});
</script>
<!-- ======================================================================= -->
<!-- colobox    -->
<!-- ======================================================================= -->

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?4KhbufEE4KrWrDWqP4QPlxQurGzLqEGf";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
