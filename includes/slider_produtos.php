<?php
$qtd_itens_linha = 3; // quantidade de itens por linha
$id_slider = "carousel-example-generic-clientes";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="carousel slide top40 slider-parceiros-clientes" data-ride="carousel" interval="555" pause="hover">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_produtos","and id_categoriaproduto IN(75) order by rand() limit 12");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_produtos", "and id_categoriaproduto IN(75) limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>
        <div class="item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>

              <div class="lista_parceiros_clientes">

                <div class="col-xs-4 div_personalizada">

                  <div class="thumbnail">
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($linha[$c_temp][url_amigavel]) ?>" title="<?php Util::imprime($linha[$c_temp][titulo]) ?>">

                      <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 340, 180, array("class"=>"top5", "alt"=>"$linha[$c_temp][titulo]") ) ?>

                    </a>
                    <div class="caption">
                      <h1><?php Util::imprime($linha[$c_temp][titulo]) ?></h1>

                      <p><?php Util::imprime($linha[$c_temp][descricao], 300) ?></p>
                      <div class="top15 font-futura">

                        <?php if ($linha[$c_temp][pode_ser_vendido] == 'SIM'): ?>
                            <a class="btn btn_saiba_mais_produtos right10" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto'" data-toggle="tooltip"  title="Comprar">
                              COMPRAR
                            </a>
                        <?php endif; ?>

                        <?php if ($linha[$c_temp][pode_ser_alugado] == 'SIM'): ?>
                        <a class="btn btn_saiba_mais_produtos" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto'"   data-toggle="tooltip"  title="Alugar">
                          ALUGAR
                        </a>
                        <?php endif; ?>

                      </div>
                      <a class="btn btn_saiba_mais_home top10" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($linha[$c_temp][url_amigavel]) ?>"  title="Saiba Mais">
                        SAIBA MAIS <i class="fa fa-angle-right left10"></i>
                      </a>
                    </div>
                  </div>


                </div>
              </div>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>



  <!-- Controls -->
  <a class="left carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <i class="fa fa-chevron-left" aria-hidden="true"></i>
  </a>
  <a class="right carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <i class="fa fa-chevron-right" aria-hidden="true"></i>
  </a>
</div>
